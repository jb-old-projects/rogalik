#pragma once
#include "Window.h"
#include "Map.h"
#include "AnimatedGameObject.h"
#include "GameObject.h"
#include "Tile.h"
#include "AnimatedTile.h"
#include "LTimer.h"
#include "Player.h"
#include <SDL2\SDL_image.h>
#include <SDL2\SDL_mixer.h>
#include <SDL2\SDL_ttf.h>


#pragma comment (lib, "SDL2_mixer")
#pragma comment (lib, "SDL2")
#pragma comment (lib, "SDL2_image")
#pragma comment (lib, "SDL2_ttf")

class Game
{
public:

	Game::Game();
	Game::~Game();
	Map *GameMap = NULL;
	Window *GameWindow = NULL;
	Player *Hero = NULL;

	Tile *GameFloor = NULL;
	Tile *GameUpWall = NULL;
	Tile *GameLeftWall = NULL;
	Tile *GameRightWall = NULL;
	Tile *GameDownWall = NULL;
	Tile *FirstCorner = NULL;
	Tile *SecondCorner = NULL;
	Tile *ThirdCorner = NULL; 
	Tile *FourthCorner = NULL; 
	AnimatedTile *GameUpDoors = NULL;
	AnimatedTile *GameLeftDoors = NULL;
	AnimatedTile *GameRightDoors = NULL;
	AnimatedTile *GameDownDoors = NULL;
	AnimatedTile *GameMenu = NULL;
	AnimatedTile *GameEnd = NULL;
	AnimatedTile *GameMenuDot = NULL;
	AnimatedTile *GameMenuPauseDot = NULL;
	AnimatedTile *GameMenuPause = NULL;

	AnimatedTile *GameResolutions = NULL;
	AnimatedTile *FullscreenOnOff = NULL;
	Tile *FloorBlackHole = NULL;
	Tile *GameBackground = NULL;
	AnimatedTile *PlayerStats = NULL;
	AnimatedTile *TextBackground = NULL;

	GameObject *Hole_1 = NULL;
	GameObject *Hole_2 = NULL;
	GameObject *Hole_3 = NULL;
	GameObject *Hole_4 = NULL;
	GameObject *Hole_5 = NULL;
	GameObject *Hole_6 = NULL;
	GameObject *Hole_7 = NULL;
	GameObject *Hole_8 = NULL;
	GameObject *Hole_9 = NULL;
	GameObject *Hole_10 = NULL;
	GameObject *Hole_11 = NULL;
	GameObject *Hole_12 = NULL;
	GameObject *Hole_13 = NULL;
	GameObject *Hole_14 = NULL; 
	
	AnimatedGameObject *Money = NULL;
	AnimatedGameObject *Heart = NULL;

	int DotW;
	int DotH;

	enum E_GameStatus
	{
		e_menu,
		e_game,
		e_gamepause,
		e_end
	};

	enum E_GameResolution
	{
		e_800x600,
		e_1024x768,
		e_1280x960,
		e_1440x1080
	};

	E_GameStatus GameStatus;
	E_GameResolution GameResolution;

	void GameLoop(E_GameStatus GameStatus);
	void Draw(E_GameStatus GameStatus);
	void DrawObjects();
	void MenuDraw();
	void MenuPauseDraw();
	void GameEnding();
	void Input(SDL_Event event, E_GameStatus GameStatus);
	void ChangeResolution(int state);
	void AddColliders();
	bool IsCollision();
	void GameInit();

	bool canWalk(int x);
	bool IsOpened;
	bool Started = 0;

	TTF_Font *gFont = NULL;

	LTimer fpsTimer;
	LTimer capTimer;

	int countedFrames;

	const int SCREEN_FPS = 60;
	const int SCREEN_TICK_PER_FRAME = 1000 / SCREEN_FPS;

	Mix_Music *gMusic = NULL;
	Mix_Music *gGameMusic = NULL;
	Mix_Chunk *gCoin = NULL;
	Mix_Chunk *gHeart = NULL;

};