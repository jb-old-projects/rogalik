#pragma once
#include <SDL.h>
#include <SDL2\SDL_image.h>
#pragma comment (lib, "SDL2_image")
#include <string>

using namespace std;

class GameObject
{
public:

	enum E_GameObjectID
	{
		e_money = 100,
		e_heart = 101,
		e_hole_1,
		e_hole_2,
		e_hole_3,
		e_hole_4,
		e_hole_5,
		e_hole_6,
		e_hole_7,
		e_hole_8,
		e_hole_9,
		e_hole_10,
		e_hole_11,
		e_hole_12,
		e_hole_13,
		e_hole_14
	};

	enum E_GameObjectType
	{
		e_neutral,
		e_harmfull,
		e_deadly,
	};

	SDL_RWops *rwop = NULL;
	E_GameObjectID GameObjectID;
	E_GameObjectType GameObjectType;

	SDL_Surface *GameObjectSurface = NULL;

	struct GameObjectCollider
	{
		int ColliderPosX;
		int ColliderPosY;
		int ColliderWidth;
		int ColliderHeight;

		void AddCollider(int _X, int _Y, int _W, int _H);

	};

	GameObjectCollider Collider;

	SDL_Rect GameObjectRect;

	void EditSurface(string bmpName, int GameObjectW, int GameObjectH);

	GameObject(E_GameObjectID _ID, E_GameObjectType _GameObjectType, string _bmpName, int _GameObjectW, int _GameObjectH);

	virtual ~GameObject();
};