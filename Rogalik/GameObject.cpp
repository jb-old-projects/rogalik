#include "GameObject.h"

GameObject::GameObject(E_GameObjectID _ID, E_GameObjectType _GameObjectType, string _bmpName, int _GameObjectW, int _GameObjectH)
{
	GameObjectID = _ID;
	GameObjectType = _GameObjectType;
	rwop = SDL_RWFromFile(_bmpName.c_str(), "br");
	GameObjectSurface = IMG_LoadPNG_RW(rwop);
	
	GameObjectRect.w = _GameObjectW;
	GameObjectRect.h = _GameObjectH;
}

GameObject::~GameObject()
{
	SDL_RWclose(rwop);
	SDL_FreeSurface(GameObjectSurface);
}

void GameObject::EditSurface(string bmpName, int W, int H) // funkcja edytuj�ca obecny surface obiektu
{
	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	GameObjectSurface = IMG_LoadPNG_RW(rwop);

	GameObjectRect.w = W;
	GameObjectRect.h = H;
}