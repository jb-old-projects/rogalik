#include "Map.h"

int Map::RoomAmount = 0;

Map::Map()
{
	TileSizeW = 72;
	TileSizeH = 54;
	TileAmount = 11;
	MapH = TileSizeH * TileAmount;
	MapW = TileSizeW * TileAmount;
	MapSize = MapH * MapW;
	CurrentRoom = new Room(99, 0);
}

Map::~Map()
{
	delete CurrentRoom;
}

Map::Room::Room(int ID, int GameStatus)
{
	RoomAmount++;
	int TileAmount = 11;
	for (int i = 0; i < TileAmount; i++)
	{
		for (int j = 0; j < TileAmount; j++)
			RoomMap[i][j] = 99;;
	}

	srand(time(0));
	int TMP;
	int TMP2 = 1;
	int TMPtab[4]{ -1 };

	if (RoomAmount > 15)
	{

		if (Door[0] == NULL && Door[1] == NULL && Door[2] == NULL && Door[3] == NULL)
		{
			TMP = rand() % 1001;
			if (TMP <= 800)
				TMP2++;
		}
	} 
	else 
		TMP2++;
	TMP = rand() % 1001;
	if (TMP <= 690)
		TMP2++;
	TMP = rand() % 1001;
	if (TMP <= 450)
		TMP2++;

	for (int i = 0; i < TileAmount; i++)
	{
		for (int j = 0; j < TileAmount; j++)
		{
			if (ID == 5 && i == 0 && j == 5)
			{
				RoomMap[j][i] = 5;
				TMP2--;
				TMPtab[0] = 1;
			}
			else if(ID == 6 && i == 5 && j == 0)
			{
				RoomMap[j][i] = 6;
				TMP2--;
				TMPtab[1] = 1;
			}
			else if (ID == 7 && i == 5 && j == 10)
			{
				RoomMap[j][i] = 7;
				TMP2--;
				TMPtab[2] = 1;
			}
			else if (ID == 8 && i == 10 && j == 5)
			{
				RoomMap[j][i] = 8;
				TMP2--;
				TMPtab[3] = 1;
			}
		}
	}

	TMP = rand() % 4;
	TMPtab[TMP] = 1;

	for (int i = 0; i < TMP2; i++)
	{
		TMP = rand() % 4;
		TMPtab[TMP] = 1;
	}

	TMP = rand() % 1001;

	if (TMP <= 690 && RoomAmount > 1)
		GenerateHoles(TMPtab, GameStatus);


	for (int i = 0; i < TileAmount; i++)
	{
		for (int j = 0; j < TileAmount; j++)
		{
			if (RoomMap[j][i] == 99)
			{
				if (i == 0)
				{
					if (j == 5 && TMP2 > 0 && TMPtab[0] == 1)
					{
						RoomMap[j][i] = 5;
						TMP2--;
					}
					else if (j == 0)
						RoomMap[j][i] = 9;
					else if (j == 10)
						RoomMap[j][i] = 10;
					else
						RoomMap[j][i] = 1;
				}
				else if (i == 10)
				{
					if (j == 5 && TMP2 > 0 && TMPtab[3] == 1)
					{
						RoomMap[j][i] = 8;
						TMP2--;
					}
					else if (j == 0)
						RoomMap[j][i] = 11;
					else if (j == 10)
						RoomMap[j][i] = 12;
					else
						RoomMap[j][i] = 4;
				}
				else if (j == 0)
				{
					if (i == 5 && TMP2 > 0 && TMPtab[1] == 1)
					{
						RoomMap[j][i] = 6;
						TMP2--;
					}
					else
						RoomMap[j][i] = 2;
				}
				else if (j == 10)
				{
					if (i == 5 && TMP2 > 0 && TMPtab[2] == 1)
					{
						RoomMap[j][i] = 7;
						TMP2--;
					}
					else
						RoomMap[j][i] = 3;
				}
				else
					RoomMap[j][i] = 0;
			}
		}
	}

	TMP = rand() % 1001;

	if (TMP <= 751)
		GenerateMoney(GameStatus);

	TMP = rand() & 1001;
		
	if (TMP <= 151)
		GenerateHearts(GameStatus);
}

Map::Room::~Room()
{
	if (Door[0] != NULL)
	{
		Door[0]->Door[3] = NULL;
		delete Door[0];
		Door[0] = NULL;
	}

	if (Door[1] != NULL)
	{
		Door[1]->Door[2] = NULL;
		delete Door[1];
		Door[1] = NULL;
	}

	if (Door[2] != NULL)
	{
		Door[2]->Door[1] = NULL;
		delete Door[2];
		Door[2] = NULL;
	}

	if (Door[3] != NULL)
	{
		Door[3]->Door[0] = NULL;
		delete Door[3];
		Door[3] = NULL;

	}
	

	RoomObjects.clear();
}

void Map::Room::GenerateHoles(int state[4], int GameStatus) // funkcja generująca dziury
{
	srand(time(0));
	int TileAmount = 11;
	int Rand_TMP;
	int EasterEggRand;

	EasterEggRand = rand() % 1001;

	Rand_TMP = 1 + rand() % 8;

	int w, h;

	if (GameStatus == 0)
	{
		w = 72;
		h = 54;
	}
	else if (GameStatus == 1)
	{
		w = 92;
		h = 69;
	}
	else if (GameStatus == 2)
	{
		w = 116;
		h = 87;
	}
	else if (GameStatus == 3)
	{
		w = 128;
		h = 96;
	}

	if (Rand_TMP == 8)
	{
		if(!(EasterEggRand <= 11))
			Rand_TMP = 5;
	}

	switch (Rand_TMP)
	{
		case 1:
		{
			RoomMap[2][2] = 18;
			RoomObjectsInfo TMP2(2 * w, 2 * h, 114);
			RoomObjects.push_back(TMP2);

			RoomMap[5][2] = 18;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 2 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[8][2] = 18;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 2 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[2][5] = 18;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[8][5] = 18;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[2][8] = 18;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[5][8] = 18;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[8][8] = 18;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			return;
			break;
		}
		case 2:
		{

			RoomMap[5][2] = 18;
			RoomObjectsInfo TMP2(5 * w, 2 * h, 114);
			RoomObjects.push_back(TMP2);

			RoomMap[2][5] = 18;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[8][5] = 18;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[5][8] = 18;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);


			return;
			break;
		}
		case 3:
		{
			RoomMap[2][2] = 18;
			RoomObjectsInfo TMP2(2 * w, 2 * h, 114);
			RoomObjects.push_back(TMP2);

			RoomMap[8][2] = 18;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 2 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[2][8] = 18;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);

			RoomMap[8][8] = 18;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 114;
			RoomObjects.push_back(TMP2);


			return;
			break;
		}
		case 4:
		{
			RoomMap[5][2] = 20;
			RoomObjectsInfo TMP2(5 * w, 2 * h, 102);
			RoomObjects.push_back(TMP2);

			RoomMap[4][3] = 24;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 106;
			RoomObjects.push_back(TMP2);

			RoomMap[5][3] = 32;

			RoomMap[6][3] = 25;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 107;
			RoomObjects.push_back(TMP2);

			RoomMap[3][4] = 24;
			TMP2.RoomObjectPosX = 3 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 106;
			RoomObjects.push_back(TMP2);

			RoomMap[4][4] = 32;

			RoomMap[5][4] = 32;

			RoomMap[6][4] = 32;


			RoomMap[7][4] = 25;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 107;
			RoomObjects.push_back(TMP2);

			RoomMap[2][5] = 21;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 103;
			RoomObjects.push_back(TMP2);

			RoomMap[3][5] = 32;

			RoomMap[4][5] = 32;

			RoomMap[5][5] = 32;

			RoomMap[6][5] = 32;

			RoomMap[7][5] = 32;

			RoomMap[8][5] = 22;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 104;
			RoomObjects.push_back(TMP2);

			RoomMap[3][6] = 26;
			TMP2.RoomObjectPosX = 3 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 108;
			RoomObjects.push_back(TMP2);

			RoomMap[4][6] = 32;

			RoomMap[5][6] = 32;

			RoomMap[6][6] = 32;

			RoomMap[7][6] = 27;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);

			RoomMap[4][7] = 26;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 108;
			RoomObjects.push_back(TMP2);

			RoomMap[5][7] = 32;

			RoomMap[6][7] = 27;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);

			RoomMap[5][8] = 23;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 105;
			RoomObjects.push_back(TMP2);

			return;
			break;
		}
		case 5:
		{
			RoomObjectsInfo TMP2(5 * w, 2 * h, 102);
			if (state[0] != 0)
			{
				RoomMap[5][1] = 0;

				RoomMap[5][2] = 0;

				RoomMap[5][3] = 0;

				RoomMap[5][4] = 0;

				RoomMap[5][5] = 0;

				RoomMap[4][1] = 30;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 1 * h;
				TMP2.RoomObjectID = 112;
				RoomObjects.push_back(TMP2);

				RoomMap[4][2] = 30;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 2 * h;
				TMP2.RoomObjectID = 112;
				RoomObjects.push_back(TMP2);

				RoomMap[4][3] = 30;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 3 * h;
				TMP2.RoomObjectID = 112;
				RoomObjects.push_back(TMP2);

				RoomMap[4][4] = 30;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 112;
				RoomObjects.push_back(TMP2);

				if (state[1] == 0)
				{
					RoomMap[4][5] = 30;

					TMP2.RoomObjectPosX = 4 * w;
					TMP2.RoomObjectPosY = 5 * h;
					TMP2.RoomObjectID = 112;
					RoomObjects.push_back(TMP2);

				}


				RoomMap[6][1] = 29;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 1 * h;
				TMP2.RoomObjectID = 111;
				RoomObjects.push_back(TMP2);

				RoomMap[6][2] = 29;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 2 * h;
				TMP2.RoomObjectID = 111;
				RoomObjects.push_back(TMP2);

				RoomMap[6][3] = 29;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 3 * h;
				TMP2.RoomObjectID = 111;
				RoomObjects.push_back(TMP2);

				RoomMap[6][4] = 29;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 111;
				RoomObjects.push_back(TMP2);

				if (state[2] == 0)
				{
					RoomMap[6][5] = 29;

					TMP2.RoomObjectPosX = 6 * w;
					TMP2.RoomObjectPosY = 5 * h;
					TMP2.RoomObjectID = 111;
					RoomObjects.push_back(TMP2);
				}
			}

			if (state[1] != 0)
			{

				RoomMap[1][5] = 0;
				RoomMap[2][5] = 0;
				RoomMap[3][5] = 0;
				RoomMap[4][5] = 0;
				RoomMap[5][5] = 0;

				RoomMap[1][4] = 31;
				TMP2.RoomObjectPosX = 1 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 113;
				RoomObjects.push_back(TMP2);

				RoomMap[2][4] = 31;
				TMP2.RoomObjectPosX = 2 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 113;
				RoomObjects.push_back(TMP2);

				RoomMap[3][4] = 31;
				TMP2.RoomObjectPosX = 3 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 113;
				RoomObjects.push_back(TMP2);

				if (RoomMap[4][4] == 30)
				{
					RoomMap[4][4] = 27;

					TMP2.RoomObjectPosX = 4 * w;
					TMP2.RoomObjectPosY = 4 * h;
					TMP2.RoomObjectID = 109;
					RoomObjects.push_back(TMP2);
				}
				if (state[0] == 0)
				{
					RoomMap[5][4] = 31;

					TMP2.RoomObjectPosX = 5 * w;
					TMP2.RoomObjectPosY = 4 * h;
					TMP2.RoomObjectID = 113;
					RoomObjects.push_back(TMP2);
				}
	

				RoomMap[1][6] = 28;
				TMP2.RoomObjectPosX = 1 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

				RoomMap[2][6] = 28;
				TMP2.RoomObjectPosX = 2 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

				RoomMap[3][6] = 28;
				TMP2.RoomObjectPosX = 3 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

				RoomMap[4][6] = 28;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

				if (state[3] == 0)
				{
					RoomMap[5][6] = 28;
					TMP2.RoomObjectPosX = 5 * w;
					TMP2.RoomObjectPosY = 6 * h;
					TMP2.RoomObjectID = 110;
					RoomObjects.push_back(TMP2);
				}
			}

			if (state[2] != 0)
			{

				RoomMap[9][5] = 0;
				RoomMap[8][5] = 0;
				RoomMap[7][5] = 0;
				RoomMap[6][5] = 0;
				RoomMap[5][5] = 0;

				RoomMap[9][4] = 31;
				TMP2.RoomObjectPosX = 9 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 113;
				RoomObjects.push_back(TMP2);

				RoomMap[8][4] = 31;
				TMP2.RoomObjectPosX = 8 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 113;
				RoomObjects.push_back(TMP2);

				RoomMap[7][4] = 31;
				TMP2.RoomObjectPosX = 7 * w;
				TMP2.RoomObjectPosY = 4 * h;
				TMP2.RoomObjectID = 113;
				RoomObjects.push_back(TMP2);

				if (RoomMap[6][4] == 29)
				{
					RoomMap[6][4] = 26;
					TMP2.RoomObjectPosX = 6 * w;
					TMP2.RoomObjectPosY = 4 * h;
					TMP2.RoomObjectID = 108;
					RoomObjects.push_back(TMP2);
				}

				if (state[3] == 0 && state[1] == 0)
				{
					RoomMap[5][6] = 28;
					TMP2.RoomObjectPosX = 5 * w;
					TMP2.RoomObjectPosY = 6 * h;
					TMP2.RoomObjectID = 110;
					RoomObjects.push_back(TMP2);
				}

				RoomMap[9][6] = 28;
				TMP2.RoomObjectPosX = 9 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

				RoomMap[8][6] = 28;
				TMP2.RoomObjectPosX = 8 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

				RoomMap[7][6] = 28;
				TMP2.RoomObjectPosX = 7 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

				RoomMap[6][6] = 28;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 6 * h;
				TMP2.RoomObjectID = 110;
				RoomObjects.push_back(TMP2);

			}

			if (state[3] != 0)
			{
				RoomMap[5][9] = 0;
				RoomMap[5][8] = 0;
				RoomMap[5][7] = 0;
				RoomMap[5][6] = 0;
				RoomMap[5][5] = 0;

				RoomMap[4][9] = 30;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 9 * h;
				TMP2.RoomObjectID = 112;
				RoomObjects.push_back(TMP2);

				RoomMap[4][8] = 30;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 8 * h;
				TMP2.RoomObjectID = 112;
				RoomObjects.push_back(TMP2);

				RoomMap[4][7] = 30;
				TMP2.RoomObjectPosX = 4 * w;
				TMP2.RoomObjectPosY = 7 * h;
				TMP2.RoomObjectID = 112;
				RoomObjects.push_back(TMP2);

				if (RoomMap[4][6] == 28)
				{
					RoomMap[4][6] = 25;
					TMP2.RoomObjectPosX = 4 * w;
					TMP2.RoomObjectPosY = 6 * h;
					TMP2.RoomObjectID = 107;
					RoomObjects.push_back(TMP2);
				}
				else
				{
					RoomMap[4][6] = 30;
					TMP2.RoomObjectPosX = 4 * w;
					TMP2.RoomObjectPosY = 6 * h;
					TMP2.RoomObjectID = 112;
					RoomObjects.push_back(TMP2);
				}

				RoomMap[6][9] = 29;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 9 * h;
				TMP2.RoomObjectID = 111;
				RoomObjects.push_back(TMP2);

				RoomMap[6][8] = 29;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 8 * h;
				TMP2.RoomObjectID = 111;
				RoomObjects.push_back(TMP2);

				RoomMap[6][7] = 29;
				TMP2.RoomObjectPosX = 6 * w;
				TMP2.RoomObjectPosY = 7 * h;
				TMP2.RoomObjectID = 111;
				RoomObjects.push_back(TMP2);

				if (RoomMap[6][6] == 28)
				{
					RoomMap[6][6] = 24;
					TMP2.RoomObjectPosX = 6 * w;
					TMP2.RoomObjectPosY = 6 * h;
					TMP2.RoomObjectID = 106;
					RoomObjects.push_back(TMP2);
				}
				else
				{
					RoomMap[6][6] = 29;
					TMP2.RoomObjectPosX = 6 * w;
					TMP2.RoomObjectPosY = 6 * h;
					TMP2.RoomObjectID = 111;
					RoomObjects.push_back(TMP2);
				}

			}

			for (int i = 1; i < TileAmount-1; i++)
			{
				for (int j = 1; j < TileAmount-1; j++)
				{
					if (RoomMap[j][i] == 99)
						RoomMap[j][i] = 32;
				}
			}

			return;
			break;
		}
		case 6:
		{
			RoomMap[2][2] = 37;
			RoomObjectsInfo TMP2(2 * w, 2 * h, 115);
			RoomObjects.push_back(TMP2);

			RoomMap[4][2] = 37;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 2 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[3][3] = 37;
			TMP2.RoomObjectPosX = 3 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[2][4] = 37;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[4][4] = 37;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);


			RoomMap[6][2] = 37;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 2 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[8][2] = 37;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 2 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[7][3] = 37;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[6][4] = 37;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[8][4] = 37;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);


			RoomMap[2][6] = 37;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[4][6] = 37;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[3][7] = 37;
			TMP2.RoomObjectPosX = 3 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[2][8] = 37;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[4][8] = 37;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);


			RoomMap[6][6] = 37;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[8][6] = 37;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[7][7] = 37;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[6][8] = 37;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);

			RoomMap[8][8] = 37;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 115;
			RoomObjects.push_back(TMP2);


			return;
			break;
		}
		case 7:
		{
			RoomMap[3][2] = 20;
			RoomObjectsInfo TMP2(3 * w, 2 * h, 102);
			RoomObjects.push_back(TMP2);

			RoomMap[7][2] = 20;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 2 * h;
			TMP2.RoomObjectID = 102;
			RoomObjects.push_back(TMP2);

			RoomMap[2][3] = 24;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 106;
			RoomObjects.push_back(TMP2);

			RoomMap[3][3] = 32;

			RoomMap[4][3] = 25;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 107;
			RoomObjects.push_back(TMP2);

			RoomMap[6][3] = 24;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 106;
			RoomObjects.push_back(TMP2);

			RoomMap[7][3] = 32;

			RoomMap[8][3] = 25;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 107;
			RoomObjects.push_back(TMP2);

			RoomMap[2][4] = 29;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 111;
			RoomObjects.push_back(TMP2);

			RoomMap[3][4] = 32;

			RoomMap[4][4] = 32;

			RoomMap[5][4] = 28;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 110;
			RoomObjects.push_back(TMP2);

			RoomMap[6][4] = 32;

			RoomMap[7][4] = 32;

			RoomMap[8][4] = 30;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 4 * h;
			TMP2.RoomObjectID = 112;
			RoomObjects.push_back(TMP2);

			RoomMap[2][5] = 26;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 108;
			RoomObjects.push_back(TMP2);

			RoomMap[3][5] = 32;

			RoomMap[4][5] = 32;

			RoomMap[5][5] = 32;

			RoomMap[6][5] = 32;

			RoomMap[7][5] = 32;

			RoomMap[8][5] = 27;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);

			RoomMap[3][6] = 26;
			TMP2.RoomObjectPosX = 3 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 108;
			RoomObjects.push_back(TMP2);

			RoomMap[4][6] = 32;

			RoomMap[5][6] = 32;

			RoomMap[6][6] = 32;

			RoomMap[7][6] = 27;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 6 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);

			RoomMap[4][7] = 26;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 108;
			RoomObjects.push_back(TMP2);

			RoomMap[5][7] = 32;

			RoomMap[6][7] = 27;
			TMP2.RoomObjectPosX = 6 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);

			RoomMap[5][8] = 23;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 8 * h;
			TMP2.RoomObjectID = 105;
			RoomObjects.push_back(TMP2);

			return;
			break;
		}
		case 8:
		{
			if (state[1] != 0)
				return;

			RoomMap[1][1] = 21;
			RoomObjectsInfo TMP2(1 * w, 1 * h, 103);
			RoomObjects.push_back(TMP2);

			RoomMap[2][1] = 25;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 1 * h;
			TMP2.RoomObjectID = 107;
			RoomObjects.push_back(TMP2);

			RoomMap[2][2] = 32;

			RoomMap[1][3] = 24;
			TMP2.RoomObjectPosX = 1 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 106;
			RoomObjects.push_back(TMP2);

			RoomMap[2][3] = 27;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);

			RoomMap[4][3] = 21;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 103;
			RoomObjects.push_back(TMP2);

			RoomMap[5][3] = 25;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 3 * h;
			TMP2.RoomObjectID = 107;
			RoomObjects.push_back(TMP2);

			RoomMap[1][4] = 32;
			RoomMap[5][4] = 32;

			RoomMap[1][5] = 26;
			TMP2.RoomObjectPosX = 1 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 108;
			RoomObjects.push_back(TMP2);

			RoomMap[2][5] = 22;
			TMP2.RoomObjectPosX = 2 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 104;
			RoomObjects.push_back(TMP2);

			RoomMap[4][5] = 21;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 103;
			RoomObjects.push_back(TMP2);

			RoomMap[5][5] = 30;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 112;
			RoomObjects.push_back(TMP2);

			RoomMap[7][5] = 24;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 106;
			RoomObjects.push_back(TMP2);

			RoomMap[8][5] = 22;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 5 * h;
			TMP2.RoomObjectID = 104;
			RoomObjects.push_back(TMP2);

			RoomMap[5][6] = 32;

			RoomMap[7][6] = 32;

			RoomMap[4][7] = 21;
			TMP2.RoomObjectPosX = 4 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 103;
			RoomObjects.push_back(TMP2);

			RoomMap[5][7] = 27;
			TMP2.RoomObjectPosX = 5 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);

			RoomMap[7][7] = 26;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 108;
			RoomObjects.push_back(TMP2);

			RoomMap[8][7] = 25;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 7 * h;
			TMP2.RoomObjectID = 107;
			RoomObjects.push_back(TMP2);

			RoomMap[8][8] = 32;

			RoomMap[7][9] = 21;
			TMP2.RoomObjectPosX = 7 * w;
			TMP2.RoomObjectPosY = 9 * h;
			TMP2.RoomObjectID = 103;
			RoomObjects.push_back(TMP2);

			RoomMap[8][9] = 27;
			TMP2.RoomObjectPosX = 8 * w;
			TMP2.RoomObjectPosY = 9 * h;
			TMP2.RoomObjectID = 109;
			RoomObjects.push_back(TMP2);
			

			break;
		}

	}

}

void Map::Room::GenerateMoney(int GameStatus) // funkcja generująca monety
{
	srand(time(0));
	int TMP;
	int x, y;
	int w, h;
	TMP = rand() % 4 + 1;

	if (GameStatus == 0)
	{
		w = 72;
		h = 54;
	}
	else if (GameStatus == 1)
	{
		w = 92;
		h = 69;
	}
	else if (GameStatus == 2)
	{
		w = 116;
		h = 87;
	}
	else if (GameStatus == 3)
	{
		w = 128;
		h = 96;
	}

	while (TMP > 0)
	{
		x = rand() % 800 + w;
		y = rand() % 600 + h;

		if (RoomMap[x / w][y / h] != 0)
			continue;

		RoomObjectsInfo TMP2(x, y, 100);
		RoomObjects.push_back(TMP2);
		TMP--;
	}
}

void Map::Room::GenerateHearts(int GameStatus) // funkcja generująca serca
{
	srand(time(0));
	int TMP = 1;
	int x, y;
	int w, h;


	if (GameStatus == 0)
	{
		w = 72;
		h = 54;
	}
	else if (GameStatus == 1)
	{
		w = 92;
		h = 69;
	}
	else if (GameStatus == 2)
	{
		w = 116;
		h = 87;
	}
	else if (GameStatus == 3)
	{
		w = 128;
		h = 96;
	}

	while (TMP > 0)
	{
		x = rand() % 800 + w;
		y = rand() % 600 + h;

		if (RoomMap[x / w][y / h] != 0)
			continue;

		RoomObjectsInfo TMP2(x, y, 101);
		RoomObjects.push_back(TMP2);
		TMP--;
	}
}
