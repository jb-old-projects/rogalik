#pragma once
#include "Tile.h"

class AnimatedTile : public Tile
{
public:

	SDL_Rect TileRect;

	AnimatedTile(E_TileID _TileID, bool _walkable, string _bmpName, int _x, int _y, int _w, int _h);
};