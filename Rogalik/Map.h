#pragma once
#include <stdlib.h>
#include <time.h>
#include "Tile.h"
#include <vector>
#include "GameObject.h"
#include "AnimatedGameObject.h"

class Map
{
public:

	Map();

	static int RoomAmount;

	int TileSizeW;
	int TileSizeH;
	int TileAmount;
	int MapH;
	int MapW;
	int MapSize;

	struct Room
	{
		Room(int ID, int GameState);
		~Room();

		int RoomMap[11][11];

		Room *Door[4]{ NULL };
		int ID = 99;

		struct RoomObjectsInfo
		{
			int RoomObjectPosX;
			int RoomObjectPosY;
			int RoomObjectID;
			int ColliderPosX;
			int ColliderPosY;
			int ColliderW;
			int ColliderH;

			RoomObjectsInfo(int _X, int _Y, int _ID)
			{
				RoomObjectID = _ID;
				RoomObjectPosX = _X;
				RoomObjectPosY = _Y;
			}
		};

		vector<RoomObjectsInfo>RoomObjects;

		void GenerateHoles(int tab[4], int GameStatus);
		void GenerateMoney(int GameStatus);
		void GenerateHearts(int GameStatus);

	};


	~Map();

	Room *CurrentRoom = NULL;

};