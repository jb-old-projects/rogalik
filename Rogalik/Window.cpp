#include "Window.h"
#include "Game.h"

Window::Window(string WindowName, int WindowWidth, int WindowHeight, bool fullScreen)
{
	gWindow = SDL_CreateWindow(WindowName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WindowWidth, WindowHeight, SDL_WINDOW_SHOWN);
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	SDL_RWops *rwop;
	rwop = SDL_RWFromFile("800x600/pngGameIcon.png", "br");
	gWindowIcon = IMG_LoadPNG_RW(rwop);

	SDL_SetWindowIcon(gWindow, gWindowIcon);
	//SDL_RWclose(rwop);
}

Window::~Window()
{
	SDL_FreeSurface(gWindowIcon);
	SDL_FreeSurface(gScreenSurface);
	 SDL_DestroyWindow(gWindow);
}
