#pragma once
#include <string>
#pragma comment (lib, "SDL2")
#pragma comment (lib, "SDL2_image")
#include <SDL_image.h>
#include <SDL.h>

using namespace std;

class Window
{
public:

	SDL_Window* gWindow = NULL;
	SDL_Surface* gScreenSurface = NULL;
	SDL_Surface* gWindowIcon = NULL;

	Window(string WindowName = "Rogalik", int WindowWidth = 792, int WindowHeight = 594, bool fullScreen = false);
	~Window();

	string WindowName;
};