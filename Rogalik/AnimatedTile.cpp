#include "AnimatedTile.h"

AnimatedTile::AnimatedTile(E_TileID _TileID, bool _walkable, string _bmpName, int _x, int _y, int _w, int _h)
	: Tile(_TileID, _walkable, _bmpName)
{
	rwop = SDL_RWFromFile(_bmpName.c_str(), "br");
	TileSurface = IMG_LoadPNG_RW(rwop);

	TileRect.x = _x;
	TileRect.y = _y;
	TileRect.w = _w;
	TileRect.h = _h;
}