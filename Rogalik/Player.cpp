#include "Player.h"

Player::Player(string _bmpName, int _PlayerPosX, int _PlayerPosY, int _HeroSizeW, int _HeroSizeH)
{
	rwop = SDL_RWFromFile(_bmpName.c_str(), "br");
	TileSurface = IMG_LoadPNG_RW(rwop);

	HeroSizeW = _HeroSizeW;
	HeroSizeH = _HeroSizeH;

	HeroAnim.x = 0;
	HeroAnim.y = 0;
	HeroAnim.w = _HeroSizeW;
	HeroAnim.h = _HeroSizeH;

	HeroPos.x = _PlayerPosX;
	HeroPos.y = _PlayerPosY;

	HeroRender.x = _PlayerPosX - (_HeroSizeW / 2);
	HeroRender.y = _PlayerPosY - _HeroSizeH;

	PlayerSpeed = 3;
	PlayerHealth = 5;
	PlayerMoney = 0;

	ColliderW = 25;
	ColliderH = 8;
	ColliderPosX_TMP = 7;
	ColliderPosY_TMP = 50;
	ColliderPosX = HeroPos.x + ColliderPosX_TMP;
	ColliderPosY = HeroPos.y + ColliderPosY_TMP;
}

Player::~Player()
{
	SDL_RWclose(rwop);
	SDL_FreeSurface(TileSurface);
}

void Player::EditSurface(string bmpName, int _HeroSizeW, int _HeroSizeH) // funkcja edytuj�ca obecny surface
{
	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	TileSurface = IMG_LoadPNG_RW(rwop);

	HeroSizeW = _HeroSizeW;
	HeroSizeH = _HeroSizeH;
	HeroAnim.w = _HeroSizeW;
	HeroAnim.h = _HeroSizeH;

	HeroRender.x = HeroPos.x - (_HeroSizeW / 2);
	HeroRender.y = HeroPos.y - _HeroSizeH;

	HeroAnim.x = 0;
	HeroAnim.y = 0;
}