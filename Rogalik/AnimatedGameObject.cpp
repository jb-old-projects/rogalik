#include "AnimatedGameObject.h"

AnimatedGameObject::AnimatedGameObject(E_GameObjectID _ID, E_GameObjectType _GameObjectType, string _bmpName, int _GameObjectW, int _GameObjectH)
	: GameObject(_ID, GameObjectType, _bmpName, _GameObjectW, _GameObjectH)
{
	GameObjectAnim.x = 0;
	GameObjectAnim.y = 0;
	GameObjectAnim.w = GameObjectRect.w;
	GameObjectAnim.h = GameObjectRect.h;
}

void AnimatedGameObject::EditSurface(string bmpName, int W, int H) // funkcja edytuj�ca obecny surface
{
	rwop = SDL_RWFromFile(bmpName.c_str(), "br");
	GameObjectSurface = IMG_LoadPNG_RW(rwop);

	GameObjectRect.w = W;
	GameObjectRect.h = H;
	GameObjectAnim.x = 0;
	GameObjectAnim.y = 0;
	GameObjectAnim.w = GameObjectRect.w;
	GameObjectAnim.h = GameObjectRect.h;

}