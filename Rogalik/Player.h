#pragma once
#include <string>
#pragma comment (lib, "SDL2_image")
#include <SDL2\SDL_image.h>

using namespace std;

class Player
{
public:

	int PlayerHealth;
	int PlayerSpeed;
	int PlayerMoney;
	int PlayerAnimationSpeed = 1;

	int HeroSizeW;
	int HeroSizeH;

	int ColliderPosX;
	int ColliderPosY;
	int ColliderW;
	int ColliderH;
	int ColliderPosX_TMP;
	int ColliderPosY_TMP;

	SDL_Rect HeroAnim;
	SDL_Rect HeroRender;
	SDL_Rect HeroPos;

	Player(string _bmpName, int _PlayerPosX, int _PlayerPosY, int _HeroSizeW, int _HeroSizeH);

	 void EditSurface(string bmpName, int HeroSizeW, int HeroSizeH);

	SDL_RWops *rwop;
	SDL_Surface *TileSurface = NULL;

	~Player();



};
