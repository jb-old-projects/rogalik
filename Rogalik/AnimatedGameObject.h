#pragma once
#include "GameObject.h"

class AnimatedGameObject : public GameObject
{ 
public:

	SDL_Rect GameObjectAnim;
	int AnimationSpeed = 1;

	void EditSurface(string bmpName, int W, int H);
	AnimatedGameObject(E_GameObjectID _ID, E_GameObjectType _GameObjectType, string _bmpName, int _GameObjectW, int _GameObjectH);


};