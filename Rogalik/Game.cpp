#include "Game.h"

/* Dodatkowo:
- dokonczyc menu
- poprawic collidery przy zmianie rozdzielczosci
- dodac nowe collidery do dziur
*/


Game::Game() // inicjalizuje pocz�tkowe warto�ci gry
{
	GameStatus = E_GameStatus::e_menu;
	GameResolution = E_GameResolution::e_800x600;
	GameWindow = new Window();
	GameMap = new Map();
	DotW = 60;
	DotH = 54;
	GameInit();
	AddColliders();
	countedFrames = 0;
	fpsTimer.start();

	IsOpened = true;
	GameLoop(GameStatus);
}

Game::~Game() // zwalnia wszystkie elementy, zapobiegaj�c wyciekom pami�ci
{
	delete GameFloor;
	delete GameUpWall;
	delete GameLeftWall;
	delete GameRightWall;
	delete GameDownWall;
	delete FirstCorner;
	delete SecondCorner;
	delete ThirdCorner;
	delete FourthCorner;
	delete GameUpDoors;
	delete GameLeftDoors;
	delete GameRightDoors;
	delete GameDownDoors;
	delete GameMenu;
	delete GameEnd;
	delete GameMenuDot;
	delete GameMenuPauseDot;
	delete GameMenuPause;
	delete GameResolutions;
	delete FullscreenOnOff;
	delete Hero;

	delete FloorBlackHole; 
	delete GameBackground;
	delete PlayerStats; 
	delete TextBackground; 
	delete Money;
	delete Heart;
	delete Hole_1;
	delete Hole_2;
	delete Hole_3;
	delete Hole_4;
	delete 	Hole_5;
	delete 	Hole_6;
	delete 	Hole_7;
	delete 	Hole_8;
	delete 	Hole_9;
	delete 	Hole_10; 
	delete 	Hole_11; 
	delete 	Hole_12;
	delete 	Hole_13;
	delete Hole_14;

	delete GameMap;
	delete GameWindow;

	fpsTimer.stop();
	capTimer.stop();

	Mix_FreeMusic(gMusic);
	Mix_FreeMusic(gGameMusic);
	Mix_FreeChunk(gCoin);
	Mix_FreeChunk(gHeart);
	Mix_CloseAudio();
	TTF_CloseFont(gFont);

	IMG_Quit();
	TTF_Quit();
	Mix_Quit();
	SDL_Quit();
}

void Game::GameInit() // funkcja �aduj�ca wszystkie potrzebne dane
{
	Hero = new Player("800x600/pngHero.png", GameMap->MapW / 2, GameMap->MapH / 2, 39, 58);
	GameFloor = new Tile(Tile::e_Floor, true, "800x600/bmpFloor2.bmp");
	GameUpWall = new Tile(Tile::e_UpperWall, false, "800x600/bmpUpsideWall.bmp");
	GameLeftWall = new Tile(Tile::e_LeftWall, false, "800x600/bmpLeftsideWall.bmp");
	GameRightWall = new Tile(Tile::e_RightWall, false, "800x600/bmpRightsideWall.bmp");
	GameDownWall = new Tile(Tile::e_BottomWall, false, "800x600/bmpDownsideWall.bmp");
	FirstCorner = new Tile(Tile::e_UpperLCorner, false, "800x600/FirstCorner.bmp");
	SecondCorner = new Tile(Tile::e_UpperRCorner, false, "800x600/SecondCorner.bmp");
	ThirdCorner = new Tile(Tile::e_BottomLCorner, false, "800x600/ThirdCorner.bmp");
	FourthCorner = new Tile(Tile::e_BottomLCorner, false, "800x600/FourthCorner.bmp");
	GameUpDoors = new AnimatedTile(AnimatedTile::e_UpperDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
	GameLeftDoors = new AnimatedTile(AnimatedTile::e_LeftDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
	GameRightDoors = new AnimatedTile(AnimatedTile::e_RightDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
	GameDownDoors = new AnimatedTile(AnimatedTile::e_BottomDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
	GameMenu = new AnimatedTile(AnimatedTile::e_menu, false, "800x600/pngGameMenu.png", 0, 0, GameMap->MapW, GameMap->MapH);
	GameEnd = new AnimatedTile(AnimatedTile::e_ending, false, "800x600/pngGameEnding.png", 0, 0, GameMap->MapW, 1440);
	GameMenuDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "800x600/pngGameMenuDot.png", 0, 0, DotW, DotH);
	GameMenuPauseDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "800x600/pngGameMenuPauseDot.png", 0, 0, DotW, DotH);
	GameMenuPause = new AnimatedTile(AnimatedTile::e_pause, false, "800x600/pngGameMenuPause.png", 0, 0, GameMap->MapW, GameMap->MapH);
	GameResolutions = new AnimatedTile(AnimatedTile::e_menu, false, "800x600/pngGameResolutions.png", 0, 0, 408, 90);
	FullscreenOnOff = new AnimatedTile(AnimatedTile::e_menu, false, "800x600/pngFulscreenOnOff.png", 0, 75, 324, 75);
	FloorBlackHole = new Tile(Tile::e_Hole, false, "800x600/bmpBlackHole.bmp");
	GameBackground = new Tile(Tile::e_Floor, false, "800x600/GameBackground.bmp");
	PlayerStats = new AnimatedTile(Tile::e_Stats, false, "800x600/pngStats.png", 0, 0, 72, 27);
	TextBackground = new AnimatedTile(Tile::e_Stats, false, "800x600/pngStatsTextBackground.png", 0, 0, 72, 27);
	/********************************************************************************************/
	Hole_1 = new GameObject(GameObject::e_hole_1, GameObject::e_neutral, "800x600/Hole_1.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_2 = new GameObject(GameObject::e_hole_2, GameObject::e_neutral, "800x600/Hole_2.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_3 = new GameObject(GameObject::e_hole_3, GameObject::e_neutral, "800x600/Hole_3.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_4 = new GameObject(GameObject::e_hole_4, GameObject::e_neutral, "800x600/Hole_4.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_5 = new GameObject(GameObject::e_hole_5, GameObject::e_neutral, "800x600/Hole_5.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_6 = new GameObject(GameObject::e_hole_6, GameObject::e_neutral, "800x600/Hole_6.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_7 = new GameObject(GameObject::e_hole_7, GameObject::e_neutral, "800x600/Hole_7.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_8 = new GameObject(GameObject::e_hole_8, GameObject::e_neutral, "800x600/Hole_8.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_9 = new GameObject(GameObject::e_hole_9, GameObject::e_neutral, "800x600/Hole_9.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_10 = new GameObject(GameObject::e_hole_10, GameObject::e_neutral, "800x600/Hole_10.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_11 = new GameObject(GameObject::e_hole_11, GameObject::e_neutral, "800x600/Hole_11.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_12 = new GameObject(GameObject::e_hole_12, GameObject::e_neutral, "800x600/Hole_12.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_13 = new GameObject(GameObject::e_hole_13, GameObject::e_neutral, "800x600/pngHole.png", GameMap->TileSizeW, GameMap->TileSizeH);
	Hole_14 = new GameObject(GameObject::e_hole_14, GameObject::e_neutral, "800x600/pngBigHole.png", GameMap->TileSizeW, GameMap->TileSizeH);

	Money = new AnimatedGameObject(AnimatedGameObject::e_money, AnimatedGameObject::e_neutral, "800x600/pngMoney.png", 32, 32);
	Heart = new AnimatedGameObject(AnimatedGameObject::e_heart, AnimatedGameObject::e_neutral, "800x600/pngHeart.png", 24, 24);

	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	gMusic = Mix_LoadMUS("music/menumusic.mp3");
	gGameMusic = Mix_LoadMUS("music/gamemusic.mp3");

	gCoin = Mix_LoadWAV("music/coin.mp3");
	gHeart = Mix_LoadWAV("music/heart.mp3");

	Mix_VolumeChunk(gHeart, 80);
	Mix_VolumeChunk(gCoin, 10);
	Mix_VolumeMusic(10);

	TTF_Init();
	gFont = TTF_OpenFont("fonts/BEECH___.ttf", 17);
}

void Game::GameLoop(Game::E_GameStatus GameStatus) // p�tla czsau rzeczywistego
{
	SDL_Event event;
	int ResChangeTMP = 0;
	int FullscreenChangeTMP = 0;
	while (IsOpened)
	{
		if (GameStatus == e_game)
		{
			Started = true;
			capTimer.start();
			while (SDL_PollEvent(&event))  // obs�uga eventu
			{
				switch (event.type)
				{
				case SDL_QUIT:
					IsOpened = false;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
						case SDLK_ESCAPE:
						{
							GameStatus = e_gamepause;
							GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.21);
							GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.36);

							break;
						}
						case SDLK_q:
						{
							GameEnding();
							break;
						}
					}
					break;
				}
			}

			if (Mix_PlayingMusic() == 0)
				Mix_PlayMusic(gGameMusic, -1);


			float avgFPS = countedFrames / (fpsTimer.getTicks() / 1000.f);

			if (avgFPS > 2000000)
				avgFPS = 0;

			Input(event, GameStatus);

			Draw(GameStatus);
			int frameTicks = capTimer.getTicks();

			if (frameTicks < SCREEN_TICK_PER_FRAME)
				SDL_Delay(SCREEN_TICK_PER_FRAME - frameTicks);
		}
	
		/******************************************            MAIN MENU            ******************************************/

		else if (GameStatus == e_menu)
		{
			int tmp = -99;
			Mix_VolumeMusic(10);
			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
				case SDL_QUIT:
					IsOpened = false;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{

						case SDLK_ESCAPE:
						{
							if (GameMenu->TileRect.y == GameMap->MapH)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.72))
									GameEnding();
								else
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.72);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.35);
								}
							}

							if (GameMenu->TileRect.y == GameMap->MapH*2)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.81))
								{
									GameMenu->TileRect.y = GameMap->MapH;
									GameMenuDot->TileRect.x = GameMap->MapW * 0.21;
									GameMenuDot->TileRect.y = GameMap->MapH * 0.21;
								}
								else
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.81);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.34);
								}
							}

							if (GameMenu->TileRect.y == GameMap->MapH * 3)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.87))
								{
									GameMenu->TileRect.y = GameMap->MapH * 2;
									GameMenuDot->TileRect.y = (GameMap->MapH*0.1);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.32);
								}
								else
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.87);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.31);
								}
							}
							
							break;
						}
						case SDLK_q:
						{
							GameEnding();
							break;
						}
						case SDLK_RETURN:
						{

							if (GameMenu->TileRect.y == 0)
							{
								GameMenu->TileRect.y = GameMap->MapH;
								GameMenuDot->TileRect.x = GameMap->MapW * 0.21;
								GameMenuDot->TileRect.y = GameMap->MapH * 0.21;
							}

							else if (GameMenu->TileRect.y == GameMap->MapH)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.21))
								{
									GameStatus = e_game;
									Mix_PauseMusic();
									Mix_PlayMusic(gGameMusic, -1);
									Mix_VolumeMusic(20);
									SDL_BlitSurface(GameBackground->TileSurface, NULL, GameWindow->gScreenSurface, NULL);
									SDL_UpdateWindowSurface(GameWindow->gWindow);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.47))
								{
									GameMenu->TileRect.y = GameMap->MapH * 2;
									GameMenuDot->TileRect.y = (GameMap->MapH*0.1);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.32);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.72))
									GameEnding();
							}
							else if (GameMenu->TileRect.y == GameMap->MapH * 2)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.1))
								{
									GameMenu->TileRect.y = GameMap->MapH * 3;
									GameMenuDot->TileRect.y = (GameMap->MapH*0.13);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.16);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.81))
								{
									if (Started == false)
									{
										GameMenu->TileRect.y = GameMap->MapH;
										GameMenuDot->TileRect.x = GameMap->MapW * 0.21;
										GameMenuDot->TileRect.y = GameMap->MapH * 0.21;
									}
									else
									{
										GameStatus = e_gamepause;
										Mix_PauseMusic();
										Mix_PlayMusic(gGameMusic, -1);
										Mix_VolumeMusic(20);
										GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.53);
										GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.31);
									}
								}
							}
							else if (GameMenu->TileRect.y == GameMap->MapH * 3)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.87))
								{
									GameMenu->TileRect.y = GameMap->MapH * 2;
									GameMenuDot->TileRect.y = (GameMap->MapH*0.1);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.32);

									int tmp2 = -99;

									if (FullscreenOnOff->TileRect.y == 0)
										tmp2 = 1;
									else
										tmp2 = 0;

									if (tmp != 0)
									{
										if (tmp2 == 1)
										{
											if (ResChangeTMP == 0 && FullscreenChangeTMP == 0)
												continue;
											ChangeResolution((GameResolutions->TileRect.y / GameResolutions->TileRect.h));
											ResChangeTMP = 0;
											FullscreenChangeTMP = 0;
											FullscreenOnOff->TileRect.y = 0;
											SDL_SetWindowFullscreen(GameWindow->gWindow, SDL_WINDOW_FULLSCREEN);
											GameWindow->gScreenSurface = SDL_GetWindowSurface(GameWindow->gWindow);
											
										}
										else if (tmp2 == 0)
										{
											if (ResChangeTMP == 0)
												continue;
											ChangeResolution((GameResolutions->TileRect.y / GameResolutions->TileRect.h));
											ResChangeTMP = 0;
											FullscreenChangeTMP = 0;
											
										}
									}
								}
							}

							break;
						}
						case SDLK_DOWN:
						{
							if (GameMenu->TileRect.y == GameMap->MapH)
							{

								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.21))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.47);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.27);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.47))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.72);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.35);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.72))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.21);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.21);
								}
							}
							
							else if (GameMenu->TileRect.y == GameMap->MapH * 2)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.1))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.35);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.32);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.35))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.58);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.24);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.58))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.81);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.34);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.81))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.1);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.32);
								}
							}

							else if (GameMenu->TileRect.y == GameMap->MapH * 3)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.13))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.57);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.225);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.57))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.87);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.31);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.87))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.13);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.16);
								}
							}

							break;
						}
						case SDLK_UP:
						{
							if (GameMenu->TileRect.y == GameMap->MapH)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.21))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.72);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.35);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.47))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.21);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.21);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.72))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.47);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.27);
								}
							}

							else if (GameMenu->TileRect.y == GameMap->MapH * 2)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.1))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.81);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.34);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.35))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.1);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.32);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.58))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.35);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.32);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.81))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.58);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.24);
								}
							}

							else if (GameMenu->TileRect.y == GameMap->MapH * 3)
							{
								if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.13))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.87);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.31);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.57))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.13);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.16);
								}
								else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.87))
								{
									GameMenuDot->TileRect.y = (GameMap->MapH*0.57);
									GameMenuDot->TileRect.x = (GameMap->MapW*0.225);
								}
							}

							break;
						}

						case SDLK_LEFT:
						{
								if (GameResolutions->TileRect.y == 0)
									tmp = 0;

								if (GameMenu->TileRect.y == GameMap->MapH * 3)
								{
									if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.13))
									{
										if (GameResolutions->TileRect.y == 0)
										{
											GameResolutions->TileRect.y = GameResolutions->TileRect.h * 3;
											ResChangeTMP--;
											if (ResChangeTMP == -4)
												ResChangeTMP = 0;
										}
										else
										{
											GameResolutions->TileRect.y -= GameResolutions->TileRect.h;
											ResChangeTMP--;

											if (ResChangeTMP == -4)
												ResChangeTMP = 0;
											tmp--;
										}
									}
									else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.57))
									{
										if (FullscreenOnOff->TileRect.y == FullscreenOnOff->TileRect.h)
										{
											FullscreenOnOff->TileRect.y = 0;
											FullscreenChangeTMP--;

											if (FullscreenChangeTMP == -2)
												FullscreenChangeTMP = 0;
										}
									}
								}

								break;
						}

						case SDLK_RIGHT:
						{
								if (GameResolutions->TileRect.y == 0)
									tmp = 0;

								if (GameMenu->TileRect.y == GameMap->MapH * 3)
								{
									if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.13))
									{
										if (GameResolutions->TileRect.y == GameResolutions->TileRect.h * 3)
										{
											GameResolutions->TileRect.y = 0;
											ResChangeTMP++;
											if (ResChangeTMP == 4)
												ResChangeTMP = 0;
										}
										
										else
										{
											GameResolutions->TileRect.y += GameResolutions->TileRect.h;
											ResChangeTMP++;

											if (ResChangeTMP == 4)
												ResChangeTMP = 0;
											tmp++;
										}
									}
									else if (GameMenuDot->TileRect.y == (int)(GameMap->MapH*0.57))
									{
										if (FullscreenOnOff->TileRect.y == 0)
										{
											FullscreenOnOff->TileRect.y = FullscreenOnOff->TileRect.h;
											FullscreenChangeTMP++;

											if (FullscreenChangeTMP == 2)
												FullscreenChangeTMP = 0;
										}


									}
								}

								break;
						}
					}
					break;
				}


			}

			if (Mix_PlayingMusic() == 0)
				Mix_PlayMusic(gMusic, 0);

			MenuDraw();
		}

		else if (GameStatus == e_gamepause)
		{

			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
				case SDL_QUIT:
					IsOpened = false;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
					case SDLK_ESCAPE:
					{
						GameStatus = e_game;
						break;
					}
					case SDLK_q:
					{
						GameEnding();
						break;
					}
					case SDLK_RETURN:
					{
						if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.36))
							GameStatus = e_game;
						else if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.53))
						{
							GameStatus = e_menu;
							Mix_HaltMusic();
							Mix_PlayMusic(gMusic, -1);
							Mix_VolumeMusic(10);
							GameMenu->TileRect.y = GameMap->MapH*2;
							GameMenuDot->TileRect.y = (GameMap->MapH*0.1);
							GameMenuDot->TileRect.x = (GameMap->MapW*0.32);
						}
						else if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.7))
							GameEnding();

						break;
					}
					case SDLK_DOWN:
					{
						if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.36))
						{
							GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.53);
							GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.31);
						}
						else if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.53))
						{
							GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.7);
							GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.36);
						}
						else if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.7))
						{
							GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.21);
							GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.36);
						}
						break;
					}
					case SDLK_UP:
					{
						if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.36))
						{
							GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.7);
							GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.36);
						}
						else if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.53))
						{
							GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.21);
							GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.36);
						}
						else if (GameMenuPauseDot->TileRect.y == (int)(GameMap->MapH*0.7))
						{
							GameMenuPauseDot->TileRect.y = (GameMap->MapH*0.53);
							GameMenuPauseDot->TileRect.x = (GameMap->MapW*0.31);
						}

						break;
					}
					}
					break;
				}
			}
			MenuPauseDraw();
		}
	}
}

void Game::Draw(E_GameStatus GameStatus) // funkcja odpowiedzialna za odpowiednie wy�wietlanie gry
{
	SDL_Rect Render_TMP;
	Render_TMP.x = 0;
	Render_TMP.y = 0;

	SDL_Color TextColor = { 255,255,255 };

	SDL_BlitSurface(GameBackground->TileSurface, NULL, GameWindow->gScreenSurface, NULL);

	for (int i = 0; i < GameMap->TileAmount; i++)
	{
		for (int j = 0; j < GameMap->TileAmount; j++)
		{
			if ((GameMap->CurrentRoom->RoomMap[j][i] > 4 && GameMap->CurrentRoom->RoomMap[j][i] < 9 ) || GameMap->CurrentRoom->RoomMap[j][i] == 0)  // floor
				SDL_BlitSurface(GameFloor->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if (GameMap->CurrentRoom->RoomMap[j][i] == 1) // up wall
				SDL_BlitSurface(GameUpWall->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if(GameMap->CurrentRoom->RoomMap[j][i] == 2) // left wall
				SDL_BlitSurface(GameLeftWall->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if(GameMap->CurrentRoom->RoomMap[j][i] == 3) // right wall
				SDL_BlitSurface(GameRightWall->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if(GameMap->CurrentRoom->RoomMap[j][i] == 4) // down wall
				SDL_BlitSurface(GameDownWall->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if(GameMap->CurrentRoom->RoomMap[j][i] == 9) // up-left corner
				SDL_BlitSurface(FirstCorner->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if (GameMap->CurrentRoom->RoomMap[j][i] == 10) // up-right corner
				SDL_BlitSurface(SecondCorner->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if (GameMap->CurrentRoom->RoomMap[j][i] == 11) // down-left corner
				SDL_BlitSurface(ThirdCorner->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			else if (GameMap->CurrentRoom->RoomMap[j][i] == 12) // down-right corner
				SDL_BlitSurface(FourthCorner->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
			if (GameMap->CurrentRoom->RoomMap[j][i] == 5) // up doors
			{
				GameUpDoors->TileRect.y = 0;
				if (GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH] == 5 && Hero->HeroPos.y < 92)
				{
					GameUpDoors->TileRect.x = GameMap->TileSizeW;
					SDL_BlitSurface(GameUpDoors->TileSurface, &GameUpDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
				else
				{
					GameUpDoors->TileRect.x = 0;
					SDL_BlitSurface(GameUpDoors->TileSurface, &GameUpDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
			}

			else if (GameMap->CurrentRoom->RoomMap[j][i] == 6) // left doors
			{
				GameLeftDoors->TileRect.y = GameMap->TileSizeH;
				if (GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH] == 6 && Hero->HeroPos.x < 92)
				{
					GameLeftDoors->TileRect.x = GameMap->TileSizeW;
					SDL_BlitSurface(GameLeftDoors->TileSurface, &GameLeftDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
				else
				{
					GameLeftDoors->TileRect.x = 0;
					SDL_BlitSurface(GameLeftDoors->TileSurface, &GameLeftDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
			}

			else if (GameMap->CurrentRoom->RoomMap[j][i] == 7) // right doors
			{
				GameRightDoors->TileRect.y = GameMap->TileSizeH*2;
				if (GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH] == 7 && Hero->HeroPos.x > 458)
				{
					GameRightDoors->TileRect.x = GameMap->TileSizeW;
					SDL_BlitSurface(GameRightDoors->TileSurface, &GameRightDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
				else
				{
					GameRightDoors->TileRect.x = 0;
					SDL_BlitSurface(GameRightDoors->TileSurface, &GameRightDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
			}

			else if (GameMap->CurrentRoom->RoomMap[j][i] == 8) // down doors
			{
				GameDownDoors->TileRect.y = GameMap->TileSizeH*3;
				if (GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH] == 8 && Hero->HeroPos.y > 458)
				{
					GameDownDoors->TileRect.x = GameMap->TileSizeW;
					SDL_BlitSurface(GameDownDoors->TileSurface, &GameDownDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
				else
				{
					GameDownDoors->TileRect.x = 0;
					SDL_BlitSurface(GameDownDoors->TileSurface, &GameDownDoors->TileRect, GameWindow->gScreenSurface, &Render_TMP);
				}
			}
			else if (GameMap->CurrentRoom->RoomMap[j][i] == 32)
					SDL_BlitSurface(FloorBlackHole->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);

			Render_TMP.x = Render_TMP.x + GameMap->TileSizeW;
		}

		Render_TMP.x = 0;
		Render_TMP.y = Render_TMP.y + GameMap->TileSizeH;
	}
	DrawObjects();
	Hero->HeroRender.x = Hero->HeroPos.x - (Hero->HeroSizeW / 2);
	Hero->HeroRender.y = Hero->HeroPos.y - Hero->HeroSizeH;
	SDL_BlitSurface(Hero->TileSurface, &Hero->HeroAnim, GameWindow->gScreenSurface, &Hero->HeroRender);
	Render_TMP.x = GameMap->TileSizeW;
	Render_TMP.y = GameMap->TileSizeH / 2;
	SDL_BlitSurface(PlayerStats->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
	Render_TMP.x += GameMap->TileSizeW * 0.625;
	Render_TMP.y += GameMap->TileSizeH * 0.11;
	char TMP_Stats[32];
	_itoa_s(Hero->PlayerHealth, TMP_Stats, 10);
	TextBackground->TileSurface = TTF_RenderText_Solid(gFont, TMP_Stats, TextColor);
	SDL_BlitSurface(TextBackground->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
	Render_TMP.x += GameMap->TileSizeW*0.9;
	_itoa_s(Hero->PlayerMoney, TMP_Stats, 10);
	TextBackground->TileSurface = TTF_RenderText_Solid(gFont, TMP_Stats, TextColor);
	SDL_BlitSurface(TextBackground->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);

	if(GameStatus == e_game)
	SDL_UpdateWindowSurface(GameWindow->gWindow);
}

void Game::DrawObjects() // funkcja odpowiedzialna za odpowiednie wy�wietlanie obiekt�w (np. monet)
{
	SDL_Rect Render_TMP;
	Render_TMP.x = 0;
	Render_TMP.y = 0;

	for(int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
	{
		if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_money)
		{
			Money->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Money->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Money->GameObjectRect.x - Money->GameObjectRect.w / 2;
			Render_TMP.y = Money->GameObjectRect.y - Money->GameObjectRect.w;
			SDL_BlitSurface(Money->GameObjectSurface, &Money->GameObjectAnim, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_heart)
		{
			Heart->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Heart->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Heart->GameObjectRect.x - Heart->GameObjectRect.w / 2;
			Render_TMP.y = Heart->GameObjectRect.y - Heart->GameObjectRect.w;
			SDL_BlitSurface(Heart->GameObjectSurface, &Heart->GameObjectAnim, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_1)
		{
			Hole_1->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_1->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_1->GameObjectRect.x;
			Render_TMP.y = Hole_1->GameObjectRect.y;
			SDL_BlitSurface(Hole_1->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_2)
		{
			Hole_2->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_2->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_2->GameObjectRect.x;
			Render_TMP.y = Hole_2->GameObjectRect.y;
			SDL_BlitSurface(Hole_2->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_3)
		{
			Hole_3->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_3->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_3->GameObjectRect.x;
			Render_TMP.y = Hole_3->GameObjectRect.y;
			SDL_BlitSurface(Hole_3->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_4)
		{
			Hole_4->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_4->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_4->GameObjectRect.x;
			Render_TMP.y = Hole_4->GameObjectRect.y;
			SDL_BlitSurface(Hole_4->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_5)
		{
			Hole_5->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_5->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_5->GameObjectRect.x;
			Render_TMP.y = Hole_5->GameObjectRect.y;
			SDL_BlitSurface(Hole_5->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_6)
		{
			Hole_6->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_6->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_6->GameObjectRect.x;
			Render_TMP.y = Hole_6->GameObjectRect.y;
			SDL_BlitSurface(Hole_6->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_7)
		{
			Hole_7->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_7->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_7->GameObjectRect.x;
			Render_TMP.y = Hole_7->GameObjectRect.y;
			SDL_BlitSurface(Hole_7->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_8)
		{
			Hole_8->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_8->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_8->GameObjectRect.x;
			Render_TMP.y = Hole_8->GameObjectRect.y;
			SDL_BlitSurface(Hole_8->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_9)
		{
			Hole_9->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_9->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_9->GameObjectRect.x;
			Render_TMP.y = Hole_9->GameObjectRect.y;
			SDL_BlitSurface(Hole_9->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_10)
		{
			Hole_10->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_10->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_10->GameObjectRect.x;
			Render_TMP.y = Hole_10->GameObjectRect.y;
			SDL_BlitSurface(Hole_10->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_11)
		{
			Hole_11->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_11->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_11->GameObjectRect.x;
			Render_TMP.y = Hole_11->GameObjectRect.y;
			SDL_BlitSurface(Hole_11->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_12)
		{
			Hole_12->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_12->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_12->GameObjectRect.x;
			Render_TMP.y = Hole_12->GameObjectRect.y;
			SDL_BlitSurface(Hole_12->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_13)
		{
			Hole_13->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_13->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_13->GameObjectRect.x;
			Render_TMP.y = Hole_13->GameObjectRect.y;
			SDL_BlitSurface(Hole_13->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
		else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_hole_14)
		{
			Hole_14->GameObjectRect.x = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX;
			Hole_14->GameObjectRect.y = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY;
			Render_TMP.x = Hole_14->GameObjectRect.x;
			Render_TMP.y = Hole_14->GameObjectRect.y;
			SDL_BlitSurface(Hole_14->GameObjectSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		}
	}

	Money->AnimationSpeed++;
	Heart->AnimationSpeed++;

	if (Money->AnimationSpeed == 10)
	{
		Money->GameObjectAnim.x += Money->GameObjectRect.w;
		Money->AnimationSpeed = 1;
	}

	if (Money->GameObjectAnim.x > Money->GameObjectRect.w*7)
		Money->GameObjectAnim.x = 0;

	if (Heart->AnimationSpeed == 10)
	{
		Heart->GameObjectAnim.x += Heart->GameObjectRect.w;
		Heart->AnimationSpeed = 1;
	}

	if (Heart->GameObjectAnim.x > Heart->GameObjectRect.w * 5)
		Heart->GameObjectAnim.x = 0;
}

void Game::Input(SDL_Event event, Game::E_GameStatus GameStatus) // funkcja odpowiedzialna za wczytywanie i kalkulowanie wszelkich warto�ci
{
	const Uint8 *CurrentKeyState = SDL_GetKeyboardState(NULL);

	// character movement

	if (CurrentKeyState[SDL_SCANCODE_LEFT])
	{	
		Hero->PlayerAnimationSpeed++;
		if (Hero->PlayerAnimationSpeed == (int)((Hero->PlayerSpeed*5)/(GameResolution+1)))
		{
			Hero->HeroAnim.x += Hero->HeroSizeW;
			Hero->PlayerAnimationSpeed = 1;
		}
		Hero->HeroAnim.y = Hero->HeroSizeH*3;

		if (Hero->HeroAnim.x > Hero->HeroSizeW*3)
			Hero->HeroAnim.x = 0;
	
		Hero->HeroPos.x -= Hero->PlayerSpeed;
		IsCollision();


		if (!canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
			Hero->HeroPos.x = ((int)(Hero->HeroPos.x / GameMap->TileSizeW)) * GameMap->TileSizeW + GameMap->TileSizeW + 1;
		else if (canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
		{
			if (Hero->HeroPos.x < GameMap->TileSizeW)
			{
				if (Hero->HeroPos.x < (GameMap->TileSizeW / 4))                                                                    
				{
					if (GameMap->CurrentRoom->Door[1] == NULL)
					{
						GameMap->CurrentRoom->Door[1] = new Map::Room(7, (int)GameResolution);

						GameMap->CurrentRoom->Door[1]->Door[2] = new Map::Room(99, (int)GameResolution);
						GameMap->CurrentRoom->Door[1]->Door[2] = GameMap->CurrentRoom;
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[1];
						AddColliders();
						GameLeftDoors->TileRect.x = 0;
						Hero->HeroPos.x = (GameMap->MapW - (GameMap->TileSizeW / 3));                                                                
					}
					else
					{
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[1];
						GameLeftDoors->TileRect.x = 0;
						Hero->HeroPos.x = (GameMap->MapW - (GameMap->TileSizeW / 3));
					}
				}
			}
		}
		Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
	}
	if (CurrentKeyState[SDL_SCANCODE_RIGHT])
	{
		Hero->PlayerAnimationSpeed++;
		if (Hero->PlayerAnimationSpeed == (int)((Hero->PlayerSpeed * 5) / (GameResolution + 1)))
		{
			Hero->HeroAnim.x += Hero->HeroSizeW;
			Hero->PlayerAnimationSpeed = 1;
		}
		Hero->HeroAnim.y = Hero->HeroSizeH*2;

		if (Hero->HeroAnim.x > Hero->HeroSizeW * 3)
			Hero->HeroAnim.x = 0;

		Hero->HeroPos.x += Hero->PlayerSpeed;
		IsCollision();

		if (!canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
			Hero->HeroPos.x = ((int)(Hero->HeroPos.x / GameMap->TileSizeW)) * GameMap->TileSizeW - 1;

		else if (canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
		{
			if (Hero->HeroPos.x >  (GameMap->MapW - GameMap->TileSizeW/3))																		
			{																								
				if (Hero->HeroPos.x > (GameMap->MapW - (GameMap->TileSizeW / 3)))																	
				{
					if (GameMap->CurrentRoom->Door[2] == NULL)
					{
						GameMap->CurrentRoom->Door[2] = new Map::Room(6, (int)GameResolution);
						GameMap->CurrentRoom->Door[2]->Door[1] = new Map::Room(99, (int)GameResolution);
						GameMap->CurrentRoom->Door[2]->Door[1] = GameMap->CurrentRoom;
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[2];
						AddColliders();
						GameRightDoors->TileRect.x = 0;
						Hero->HeroPos.x = (GameMap->TileSizeW/4);																	
					}
					else
					{
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[2];
						GameRightDoors->TileRect.x = 0;
						Hero->HeroPos.x = (GameMap->TileSizeW / 4);
					}
				}

			}
		}
		Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
	}

	if (CurrentKeyState[SDL_SCANCODE_UP])
	{
		Hero->PlayerAnimationSpeed++;
		if (Hero->PlayerAnimationSpeed == (int)((Hero->PlayerSpeed * 5) / (GameResolution + 1)))
		{
			Hero->HeroAnim.x += Hero->HeroSizeW;
			Hero->PlayerAnimationSpeed = 1;
		}
		Hero->HeroAnim.y = Hero->HeroSizeH;

		if (Hero->HeroAnim.x > Hero->HeroSizeW * 3)
			Hero->HeroAnim.x = 0;

		Hero->HeroPos.y -= Hero->PlayerSpeed;
		IsCollision();

		if (!canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
			Hero->HeroPos.y = ((int)(Hero->HeroPos.y / GameMap->TileSizeH))*GameMap->TileSizeH + GameMap->TileSizeH + 1;
		else if (canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
		{
			if (Hero->HeroPos.y < GameMap->TileSizeH)
			{
				if (Hero->HeroPos.y < (GameMap->TileSizeH / 3))
				{
					if (GameMap->CurrentRoom->Door[0] == NULL)
					{
						GameMap->CurrentRoom->Door[0] = new Map::Room(8, (int)GameResolution);
						GameMap->CurrentRoom->Door[0]->Door[3] = new Map::Room(99, (int)GameResolution);
						GameMap->CurrentRoom->Door[0]->Door[3] = GameMap->CurrentRoom;
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[0];
						AddColliders();
						GameUpDoors->TileRect.x = 0;
						Hero->HeroPos.y = (GameMap->MapH - (GameMap->TileSizeH / 3));
					}
					else
					{
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[0];
						GameUpDoors->TileRect.x = 0;
						Hero->HeroPos.y = (GameMap->MapH - (GameMap->TileSizeH / 3));
					}
				}
			}
		}
		Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
	}
	if (CurrentKeyState[SDL_SCANCODE_DOWN])
	{
		Hero->PlayerAnimationSpeed++;
		if (Hero->PlayerAnimationSpeed == (int)((Hero->PlayerSpeed * 5) / (GameResolution + 1)))
		{
			Hero->HeroAnim.x += Hero->HeroSizeW;
			Hero->PlayerAnimationSpeed = 1;
		}
		Hero->HeroAnim.y = 0;

		if (Hero->HeroAnim.x > Hero->HeroSizeW * 3)
			Hero->HeroAnim.x = 0;

		Hero->HeroPos.y += Hero->PlayerSpeed;
		IsCollision();

		if (!canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
			Hero->HeroPos.y = ((int)(Hero->HeroPos.y / GameMap->TileSizeH)) * GameMap->TileSizeH - 1;

		else if (canWalk(GameMap->CurrentRoom->RoomMap[Hero->HeroPos.x / GameMap->TileSizeW][Hero->HeroPos.y / GameMap->TileSizeH]))
		{
			if (Hero->HeroPos.y > (GameMap->MapH - (GameMap->TileSizeH / 3)))
			{
				
				if (Hero->HeroPos.y > (GameMap->MapH - (GameMap->TileSizeH / 8)))
				{
					if (GameMap->CurrentRoom->Door[3] == NULL)
					{
						GameMap->CurrentRoom->Door[3] = new Map::Room(5, (int)GameResolution);
						GameMap->CurrentRoom->Door[3]->Door[0] = new Map::Room(99, (int)GameResolution);
						GameMap->CurrentRoom->Door[3]->Door[0] = GameMap->CurrentRoom;
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[3];
						AddColliders();
						GameDownDoors->TileRect.x = 0;
						Hero->HeroPos.y = (GameMap->TileSizeH / 2);
					}
					else
					{
						GameMap->CurrentRoom = GameMap->CurrentRoom->Door[3];
						GameDownDoors->TileRect.x = 0;
						Hero->HeroPos.y = (GameMap->TileSizeH / 2);
					}
				}
			}
		}
		Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
	}
}

bool Game::canWalk(int x)
{
	if (x == 0)
		return true;
	else if (x > 0 && x < 5)
		return false;
	else if (x > 4 && x < 9)
		return true;
	else if (x >= 18 && x <= 37)
		return false;
}

void Game::MenuDraw()  // funkcja odpowiedzialna za odpowiednie wy�wietlanie g��wnego menu gry
{
	SDL_Rect Render_TMP;
	Render_TMP.x = 0;
	Render_TMP.y = 0;

	if (GameMenu->TileRect.y < GameMap->MapH*3 && GameMenu->TileRect.y != 0)
	{
		SDL_BlitSurface(GameMenu->TileSurface, &GameMenu->TileRect, GameWindow->gScreenSurface, &Render_TMP);
		SDL_BlitSurface(GameMenuDot->TileSurface, NULL , GameWindow->gScreenSurface, &GameMenuDot->TileRect);
		SDL_UpdateWindowSurface(GameWindow->gWindow);
		return;
	}

	else if (GameMenu->TileRect.y >= GameMap->MapH * 3)
	{
		SDL_Rect Render_TMP2;
		Render_TMP2.x = (GameMap->MapW*0.26);
		Render_TMP2.y = (GameMap->MapH*0.1);

		SDL_BlitSurface(GameMenu->TileSurface, &GameMenu->TileRect, GameWindow->gScreenSurface, &Render_TMP);
		SDL_BlitSurface(GameMenuDot->TileSurface, NULL, GameWindow->gScreenSurface, &GameMenuDot->TileRect);
		SDL_BlitSurface(GameResolutions->TileSurface, &GameResolutions->TileRect, GameWindow->gScreenSurface, &Render_TMP2);
		Render_TMP2.x = (GameMap->MapW*0.29);
		Render_TMP2.y = (GameMap->MapH*0.54);
		SDL_BlitSurface(FullscreenOnOff->TileSurface, &FullscreenOnOff->TileRect, GameWindow->gScreenSurface, &Render_TMP2);
		SDL_UpdateWindowSurface(GameWindow->gWindow);
		return;
	}

	SDL_BlitSurface(GameMenu->TileSurface, &GameMenu->TileRect, GameWindow->gScreenSurface, &Render_TMP);
	SDL_UpdateWindowSurface(GameWindow->gWindow);

}

void Game::GameEnding() // funkcja odpowiedzialna za wy�wietlenie ko�cowego menu gry oraz rozpocz�cie zwalniania pami�ci
{
	SDL_Rect Render_TMP;
	Render_TMP.x = 0;
	Render_TMP.y = 0;
	SDL_BlitSurface(GameEnd->TileSurface, &GameEnd->TileRect, GameWindow->gScreenSurface, &Render_TMP);
	SDL_UpdateWindowSurface(GameWindow->gWindow);

	int i = 0;

	SDL_Event event;

	SDL_Delay(1000);

	while (GameEnd->TileRect.y < (GameEnd->TileRect.h - GameMap->MapH*1.05))
	{
		SDL_BlitSurface(GameEnd->TileSurface, &GameEnd->TileRect, GameWindow->gScreenSurface, &Render_TMP);
		SDL_UpdateWindowSurface(GameWindow->gWindow);
		GameEnd->TileRect.y++;

		SDL_Delay(2);

		if (GameEnd->TileRect.y == (GameEnd->TileRect.h - GameMap->MapH))
		{
			SDL_Delay(1000);
			break;
		}

		while (SDL_PollEvent(&event)) 
		{
			switch (event.type)
			{
			case SDL_QUIT:
				i = 99;
			break;

			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
				{
					i = 99;
					break;
				}
				case SDLK_q:
				{
					i = 99;
					break;
				}
				}
			break;
			}

		}
		if (i == 99)
			break;
	}

	if(i!=99)
	SDL_Delay(2500);

	IsOpened = false;
}

void Game::MenuPauseDraw() // funkcja odpowiedzialna za odpowiednie wy�wietlanie menu w trakcie pauzy gry
{
	SDL_Rect Render_TMP;
	Render_TMP.x = 0;
	Render_TMP.y = 0;
	Render_TMP.w = GameMap->MapW;
	Render_TMP.h = GameMap->MapH;

		Draw(GameStatus);

		SDL_BlitSurface(GameMenuPause->TileSurface, NULL, GameWindow->gScreenSurface, &Render_TMP);
		SDL_BlitSurface(GameMenuPauseDot->TileSurface, NULL, GameWindow->gScreenSurface, &GameMenuPauseDot->TileRect);
		SDL_UpdateWindowSurface(GameWindow->gWindow);
}

void Game::ChangeResolution(int state) // funkcja odpowiedzialna za odpowiedni� zmian� rozdzielczo�ci gry (w tym za�adowanie od nowa odpowiednich danych)
{
	delete GameFloor;
	delete GameUpWall;
	delete GameLeftWall;
	delete GameRightWall;
	delete GameDownWall;
	delete FirstCorner;
	delete SecondCorner;
	delete ThirdCorner;
	delete FourthCorner;
	delete GameUpDoors;
	delete GameLeftDoors;
	delete GameRightDoors;
	delete GameDownDoors;
	delete GameMenu;
	delete GameEnd;
	delete GameMenuDot;
	delete GameMenuPauseDot;
	delete GameMenuPause;
	delete GameResolutions;
	delete FullscreenOnOff;
	delete FloorBlackHole;
	delete GameBackground;
	delete PlayerStats;
	delete TextBackground;

	if (state == 0)
	{
		if (GameResolution == e_1024x768)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*0.783;
			Hero->HeroPos.y = Hero->HeroPos.y*0.783;
			Hero->ColliderPosX_TMP *= 0.783;
			Hero->ColliderPosY_TMP *= 0.783;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 0.783;
			Hero->ColliderW *= 0.783;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 0.783;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 0.783;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 0.783;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 0.783;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 0.783;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 0.783;
			}
		}
		else if (GameResolution == e_1280x960)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*0.621;
			Hero->HeroPos.y = Hero->HeroPos.y*0.621;
			Hero->ColliderPosX_TMP *= 0.621;
			Hero->ColliderPosY_TMP *= 0.621;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 0.621;
			Hero->ColliderW *= 0.621;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 0.62;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 0.62;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 0.621;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 0.621;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 0.621;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 0.621;
			}
		}
		else if (GameResolution == e_1440x1080)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*0.563;
			Hero->HeroPos.y = Hero->HeroPos.y*0.563;
			Hero->ColliderPosX_TMP *= 0.563;
			Hero->ColliderPosY_TMP *= 0.563;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 0.563;
			Hero->ColliderW *= 0.563;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 0.563;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 0.563;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 0.563;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 0.563;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 0.563;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 0.563;
			}
		}

		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 17);
		GameResolution = e_800x600;

		GameMap->TileSizeW = 72;
		GameMap->TileSizeH = 54;
		GameMap->TileAmount = 11;
		GameMap->MapH = GameMap->TileSizeH * GameMap->TileAmount;
		GameMap->MapW = GameMap->TileSizeW * GameMap->TileAmount;
		GameMap->MapSize = GameMap->MapH * GameMap->MapW;

		Hero->EditSurface("800x600/pngHero.png", 39, 58);
		Hero->PlayerSpeed = 3;
		Hero->PlayerAnimationSpeed = 1;
		DotW = 60;
		DotH = 54;
		
		Money->EditSurface("800x600/pngMoney.png", 32, 32);
		Hole_1->EditSurface("800x600/Hole_1.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_2->EditSurface("800x600/Hole_2.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_3->EditSurface("800x600/Hole_3.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_4->EditSurface("800x600/Hole_4.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_5->EditSurface("800x600/Hole_5.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_6->EditSurface("800x600/Hole_6.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_7->EditSurface("800x600/Hole_7.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_8->EditSurface("800x600/Hole_8.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_9->EditSurface("800x600/Hole_9.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_10->EditSurface("800x600/Hole_10.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_11->EditSurface("800x600/Hole_11.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_12->EditSurface("800x600/Hole_12.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_13->EditSurface("800x600/pngHole.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_14->EditSurface("800x600/pngBigHole.png", GameMap->TileSizeW, GameMap->TileSizeH);


		GameFloor = new Tile(Tile::e_Floor, true, "800x600/bmpFloor2.bmp");
		GameUpWall = new Tile(Tile::e_UpperWall, false, "800x600/bmpUpsideWall.bmp");
		GameLeftWall = new Tile(Tile::e_LeftWall, false, "800x600/bmpLeftsideWall.bmp");
		GameRightWall = new Tile(Tile::e_RightWall, false, "800x600/bmpRightsideWall.bmp");
		GameDownWall = new Tile(Tile::e_BottomWall, false, "800x600/bmpDownsideWall.bmp");
		FirstCorner = new Tile(Tile::e_UpperLCorner, false, "800x600/FirstCorner.bmp");
		SecondCorner = new Tile(Tile::e_UpperRCorner, false, "800x600/SecondCorner.bmp");
		ThirdCorner = new Tile(Tile::e_BottomLCorner, false, "800x600/ThirdCorner.bmp");
		FourthCorner = new Tile(Tile::e_BottomLCorner, false, "800x600/FourthCorner.bmp");
		GameUpDoors = new AnimatedTile(AnimatedTile::e_UpperDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameLeftDoors = new AnimatedTile(AnimatedTile::e_LeftDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameRightDoors = new AnimatedTile(AnimatedTile::e_RightDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameDownDoors = new AnimatedTile(AnimatedTile::e_BottomDoors, true, "800x600/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameMenu = new AnimatedTile(AnimatedTile::e_menu, false, "800x600/pngGameMenu.png", 0, GameMap->MapH*2, GameMap->MapW, GameMap->MapH);
		GameEnd = new AnimatedTile(AnimatedTile::e_ending, false, "800x600/pngGameEnding.png", 0, 0, GameMap->MapW, 1440);
		GameMenuDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "800x600/pngGameMenuDot.png", (GameMap->MapW*0.32), (GameMap->MapH*0.1), DotW, DotH);
		GameMenuPauseDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "800x600/pngGameMenuPauseDot.png", 0, 0, DotW, DotH);
		GameMenuPause = new AnimatedTile(AnimatedTile::e_pause, false, "800x600/pngGameMenuPause.png", 0, 0, GameMap->MapW, GameMap->MapH);
		GameResolutions = new AnimatedTile(AnimatedTile::e_menu, false, "800x600/pngGameResolutions.png", 0, 0, 408, (360 / 4));
		FullscreenOnOff = new AnimatedTile(AnimatedTile::e_menu, false, "800x600/pngFulscreenOnOff.png", 0, 75, 324, (150 / 2));
		FloorBlackHole = new Tile(Tile::e_Hole, true, "800x600/bmpBlackHole.bmp");
		GameBackground = new Tile(Tile::e_Floor, false, "800x600/GameBackground.bmp");
		PlayerStats = new AnimatedTile(Tile::e_Stats, false, "800x600/pngStats.png", 0, 0, 72, 27);
		TextBackground = new AnimatedTile(Tile::e_Stats, false, "800x600/pngStatsTextBackground.png", 0, 0, 72, 27);

		delete GameWindow;
		GameWindow = new Window("Rogalik", 792, 594, false);
		GameWindow->gScreenSurface = SDL_GetWindowSurface(GameWindow->gWindow);
		return;
	}
	else if (state == 1)
	{
		if (GameResolution == e_800x600)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*1.278;
			Hero->HeroPos.y = Hero->HeroPos.y*1.278;
			Hero->ColliderPosX_TMP *= 1.278;
			Hero->ColliderPosY_TMP *= 1.278;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 1.278;
			Hero->ColliderW *= 1.278;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 1.278;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 1.278;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15*1.278 ;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48*1.278 ;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 1.278;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 1.278;
			}
		}
		else if (GameResolution == e_1280x960)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*0.794;
			Hero->HeroPos.y = Hero->HeroPos.y*0.794;
			Hero->ColliderPosX_TMP *= 0.794;
			Hero->ColliderPosY_TMP *= 0.794;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 0.794;
			Hero->ColliderW *= 0.794;
			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 0.794;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 0.794;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 0.794;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 0.794;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 0.794;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 0.794;
			}
		}
		else if (GameResolution == e_1440x1080)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*0.719;
			Hero->HeroPos.y = Hero->HeroPos.y*0.719;
			Hero->ColliderPosX_TMP *= 0.719;
			Hero->ColliderPosY_TMP *= 0.719;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 0.719;
			Hero->ColliderW *= 0.719;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 0.719;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 0.719;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 0.719;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 0.719;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 0.719;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 0.719;
			}
		}

		GameResolution = e_1024x768;
		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 20);


		GameMap->TileSizeW = 92;
		GameMap->TileSizeH = 69;
		GameMap->TileAmount = 11;
		GameMap->MapH = GameMap->TileSizeH * GameMap->TileAmount;
		GameMap->MapW = GameMap->TileSizeW * GameMap->TileAmount;
		GameMap->MapSize = GameMap->MapH * GameMap->MapW;
		Hero->EditSurface("1024x768/pngHero.png", 49, 74);
		Hero->PlayerSpeed = 4;
		Hero->PlayerAnimationSpeed = 1;
		DotW = 77;
		DotH = 69;

		Money->EditSurface("1024x768/pngMoney.png", 49, 37);
		Hole_1->EditSurface("1024x768/Hole_1.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_2->EditSurface("1024x768/Hole_2.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_3->EditSurface("1024x768/Hole_3.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_4->EditSurface("1024x768/Hole_4.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_5->EditSurface("1024x768/Hole_5.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_6->EditSurface("1024x768/Hole_6.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_7->EditSurface("1024x768/Hole_7.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_8->EditSurface("1024x768/Hole_8.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_9->EditSurface("1024x768/Hole_9.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_10->EditSurface("1024x768/Hole_10.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_11->EditSurface("1024x768/Hole_11.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_12->EditSurface("1024x768/Hole_12.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_13->EditSurface("1024x768/pngHole.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_14->EditSurface("1024x768/pngBigHole.png", GameMap->TileSizeW, GameMap->TileSizeH);

		GameFloor = new Tile(Tile::e_Floor, true, "1024x768/bmpFloor2.bmp");
		GameUpWall = new Tile(Tile::e_UpperWall, false, "1024x768/bmpUpsideWall.bmp");
		GameLeftWall = new Tile(Tile::e_LeftWall, false, "1024x768/bmpLeftsideWall.bmp");
		GameRightWall = new Tile(Tile::e_RightWall, false, "1024x768/bmpRightsideWall.bmp");
		GameDownWall = new Tile(Tile::e_BottomWall, false, "1024x768/bmpDownsideWall.bmp");
		FirstCorner = new Tile(Tile::e_UpperLCorner, false, "1024x768/FirstCorner.bmp");
		SecondCorner = new Tile(Tile::e_UpperRCorner, false, "1024x768/SecondCorner.bmp");
		ThirdCorner = new Tile(Tile::e_BottomLCorner, false, "1024x768/ThirdCorner.bmp");
		FourthCorner = new Tile(Tile::e_BottomLCorner, false, "1024x768/FourthCorner.bmp");
		GameUpDoors = new AnimatedTile(AnimatedTile::e_UpperDoors, true, "1024x768/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameLeftDoors = new AnimatedTile(AnimatedTile::e_LeftDoors, true, "1024x768/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameRightDoors = new AnimatedTile(AnimatedTile::e_RightDoors, true, "1024x768/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameDownDoors = new AnimatedTile(AnimatedTile::e_BottomDoors, true, "1024x768/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameMenu = new AnimatedTile(AnimatedTile::e_menu, false, "1024x768/pngGameMenu.png", 0, GameMap->MapH*2, GameMap->MapW, GameMap->MapH);
		GameEnd = new AnimatedTile(AnimatedTile::e_ending, false, "1024x768/pngGameEnding.png", 0, 0, GameMap->MapW, 1840);
		GameMenuDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "1024x768/pngGameMenuDot.png", (GameMap->MapW*0.32), (GameMap->MapH*0.1), DotW, DotH);
		GameMenuPauseDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "1024x768/pngGameMenuPauseDot.png", 0, 0, DotW, DotH);
		GameMenuPause = new AnimatedTile(AnimatedTile::e_pause, false, "1024x768/pngGameMenuPause.png", 0, 0, GameMap->MapW, GameMap->MapH);
		GameResolutions = new AnimatedTile(AnimatedTile::e_menu, false, "1024x768/pngGameResolutions.png", 0, 115, 521, (460 / 4));
		FullscreenOnOff = new AnimatedTile(AnimatedTile::e_menu, false, "1024x768/pngFulscreenOnOff.png", 0, (190 / 2), 415, (190 / 2));
		FloorBlackHole = new Tile(Tile::e_Hole, true, "1024x768/bmpBlackHole.bmp");
		GameBackground = new Tile(Tile::e_Floor, false, "1024x768/GameBackground.bmp");
		PlayerStats = new AnimatedTile(Tile::e_Stats, false, "1024x768/pngStats.png", 0, 0, 92, 34);
		TextBackground = new AnimatedTile(Tile::e_Stats, false, "1024x768/pngStatsTextBackground.png", 0, 0, 92, 34);

		delete GameWindow;
		GameWindow = new Window("Rogalik", 1012, 759, false);
		GameWindow->gScreenSurface = SDL_GetWindowSurface(GameWindow->gWindow);
		return;
	}
	else if (state == 2)
	{
		if (GameResolution == e_800x600)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*1.612;
			Hero->HeroPos.y = Hero->HeroPos.y*1.612;
			Hero->ColliderPosX_TMP *= 1.612;
			Hero->ColliderPosY_TMP *= 1.612;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 1.612;
			Hero->ColliderW *= 1.612;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 1.612;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 1.612;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 1.612;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 1.612;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 1.612;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 1.612;
			}
		}
		else if (GameResolution == e_1024x768)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*1.261;
			Hero->HeroPos.y = Hero->HeroPos.y*1.261;
			Hero->ColliderPosX_TMP *= 1.261;
			Hero->ColliderPosY_TMP *= 1.261;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 1.261;
			Hero->ColliderW *= 1.261;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 1.261;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 1.261;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 1.261;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 1.261;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 1.261;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 1.261;
			}
		}
		else if (GameResolution == e_1440x1080)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*0.907;
			Hero->HeroPos.y = Hero->HeroPos.y*0.907;
			Hero->ColliderPosX_TMP *= 0.907;
			Hero->ColliderPosY_TMP *= 0.907;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 0.907;
			Hero->ColliderW *= 0.907;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 0.907;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 0.907;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 0.907;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 0.907;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 0.907;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 0.907;
			}
		}

		GameResolution = e_1280x960;
		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 23);

		GameMap->TileSizeW = 116;
		GameMap->TileSizeH = 87;
		GameMap->TileAmount = 11;
		GameMap->MapH = GameMap->TileSizeH * GameMap->TileAmount;
		GameMap->MapW = GameMap->TileSizeW * GameMap->TileAmount;
		GameMap->MapSize = GameMap->MapH * GameMap->MapW;

		Hero->EditSurface("1280x960/pngHero.png", 62, 93);


		Hero->PlayerSpeed = 5;
		Hero->PlayerAnimationSpeed = 1;
		DotW = 97;
		DotH = 87;

		Money->EditSurface("1280x960/pngMoney.png", 62, 46);
		Hole_1->EditSurface("1280x960/Hole_1.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_2->EditSurface("1280x960/Hole_2.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_3->EditSurface("1280x960/Hole_3.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_4->EditSurface("1280x960/Hole_4.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_5->EditSurface("1280x960/Hole_5.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_6->EditSurface("1280x960/Hole_6.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_7->EditSurface("1280x960/Hole_7.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_8->EditSurface("1280x960/Hole_8.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_9->EditSurface("1280x960/Hole_9.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_10->EditSurface("1280x960/Hole_10.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_11->EditSurface("1280x960/Hole_11.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_12->EditSurface("1280x960/Hole_12.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_13->EditSurface("1280x960/pngHole.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_14->EditSurface("1280x960/pngBigHole.png", GameMap->TileSizeW, GameMap->TileSizeH);


		GameFloor = new Tile(Tile::e_Floor, true, "1280x960/bmpFloor2.bmp");
		GameUpWall = new Tile(Tile::e_UpperWall, false, "1280x960/bmpUpsideWall.bmp");
		GameLeftWall = new Tile(Tile::e_LeftWall, false, "1280x960/bmpLeftsideWall.bmp");
		GameRightWall = new Tile(Tile::e_RightWall, false, "1280x960/bmpRightsideWall.bmp");
		GameDownWall = new Tile(Tile::e_BottomWall, false, "1280x960/bmpDownsideWall.bmp");
		FirstCorner = new Tile(Tile::e_UpperLCorner, false, "1280x960/FirstCorner.bmp");
		SecondCorner = new Tile(Tile::e_UpperRCorner, false, "1280x960/SecondCorner.bmp");
		ThirdCorner = new Tile(Tile::e_BottomLCorner, false, "1280x960/ThirdCorner.bmp");
		FourthCorner = new Tile(Tile::e_BottomLCorner, false, "1280x960/FourthCorner.bmp");
		GameUpDoors = new AnimatedTile(AnimatedTile::e_UpperDoors, true, "1280x960/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameLeftDoors = new AnimatedTile(AnimatedTile::e_LeftDoors, true, "1280x960/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameRightDoors = new AnimatedTile(AnimatedTile::e_RightDoors, true, "1280x960/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameDownDoors = new AnimatedTile(AnimatedTile::e_BottomDoors, true, "1280x960/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameMenu = new AnimatedTile(AnimatedTile::e_menu, false, "1280x960/pngGameMenu.png", 0, GameMap->MapH * 2, GameMap->MapW, GameMap->MapH);
		GameEnd = new AnimatedTile(AnimatedTile::e_ending, false, "1280x960/pngGameEnding.png", 0, 0, GameMap->MapW, 2320);
		GameMenuDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "1280x960/pngGameMenuDot.png", (GameMap->MapW*0.32), (GameMap->MapH*0.1), DotW, DotH);
		GameMenuPauseDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "1280x960/pngGameMenuPauseDot.png", 0, 0, DotW, DotH);
		GameMenuPause = new AnimatedTile(AnimatedTile::e_pause, false, "1280x960/pngGameMenuPause.png", 0, 0, GameMap->MapW, GameMap->MapH);
		GameResolutions = new AnimatedTile(AnimatedTile::e_menu, false, "1280x960/pngGameResolutions.png", 0, 290, 657, (580 / 4));
		FullscreenOnOff = new AnimatedTile(AnimatedTile::e_menu, false, "1280x960/pngFulscreenOnOff.png", 0, (238 / 2), 524, (238 / 2));
		FloorBlackHole = new Tile(Tile::e_Hole, true, "1280x960/bmpBlackHole.bmp");
		GameBackground = new Tile(Tile::e_Floor, false, "1280x960/GameBackground.bmp");
		PlayerStats = new AnimatedTile(Tile::e_Stats, false, "1280x960/pngStats.png", 0, 0, 116, 43);
		TextBackground = new AnimatedTile(Tile::e_Stats, false, "1280x960/pngStatsTextBackground.png", 0, 0, 116, 43);

		delete GameWindow;
		GameWindow = new Window("Rogalik", 1276, 957, false);
		GameWindow->gScreenSurface = SDL_GetWindowSurface(GameWindow->gWindow);
		return;
	}
	else if (state == 3)
	{
		if (GameResolution == e_800x600)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*1.778;
			Hero->HeroPos.y = Hero->HeroPos.y*1.778;
			Hero->ColliderPosX_TMP *= 1.778;
			Hero->ColliderPosY_TMP *= 1.778;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 1.778;
			Hero->ColliderW *= 1.778;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 1.778;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 1.778;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 1.778;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 1.778;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 1.778;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 1.778;
			}
		}
		else if (GameResolution == e_1024x768)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*1.392;
			Hero->HeroPos.y = Hero->HeroPos.y*1.392;
			Hero->ColliderPosX_TMP *= 1.392;
			Hero->ColliderPosY_TMP *= 1.392;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 1.392;
			Hero->ColliderW *= 1.392;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 1.392;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 1.392;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 1.392;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 1.392;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 1.392;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 1.392;
			}
		}
		else if (GameResolution == e_1280x960)
		{
			Hero->HeroPos.x = Hero->HeroPos.x*1.104;
			Hero->HeroPos.y = Hero->HeroPos.y*1.104;
			Hero->ColliderPosX_TMP *= 1.104;
			Hero->ColliderPosY_TMP *= 1.104;
			Hero->ColliderPosX = Hero->HeroPos.x + Hero->ColliderPosX_TMP;
			Hero->ColliderPosY = Hero->HeroPos.y + Hero->ColliderPosY_TMP;
			Hero->ColliderH *= 1.104;
			Hero->ColliderW *= 1.104;

			for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
			{
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX *= 1.104;
				GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY *= 1.104;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15 * 1.104;
				GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48 * 1.104;
				GameMap->CurrentRoom->RoomObjects[i].ColliderW *= 1.104;
				GameMap->CurrentRoom->RoomObjects[i].ColliderH *= 1.104;
			}
		}

		GameResolution = e_1440x1080;
		TTF_CloseFont(gFont);
		gFont = TTF_OpenFont("fonts/BEECH___.ttf", 26);

		GameMap->TileSizeW = 128;
		GameMap->TileSizeH = 96;
		GameMap->TileAmount = 11;
		GameMap->MapH = GameMap->TileSizeH * GameMap->TileAmount;
		GameMap->MapW = GameMap->TileSizeW * GameMap->TileAmount;
		GameMap->MapSize = GameMap->MapH * GameMap->MapW;
		Hero->EditSurface("1440x1080/pngHero.png", 68, 102);

		Hero->PlayerSpeed = 6;
		Hero->PlayerAnimationSpeed = 1;
		DotW = 106;
		DotH = 96;

		Money->EditSurface("1440x1080/pngMoney.png", 68, 51);
		Hole_1->EditSurface("1440x1080/Hole_1.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_2->EditSurface("1440x1080/Hole_2.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_3->EditSurface("1440x1080/Hole_3.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_4->EditSurface("1440x1080/Hole_4.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_5->EditSurface("1440x1080/Hole_5.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_6->EditSurface("1440x1080/Hole_6.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_7->EditSurface("1440x1080/Hole_7.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_8->EditSurface("1440x1080/Hole_8.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_9->EditSurface("1440x1080/Hole_9.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_10->EditSurface("1440x1080/Hole_10.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_11->EditSurface("1440x1080/Hole_11.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_12->EditSurface("1440x1080/Hole_12.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_13->EditSurface("1440x1080/pngHole.png", GameMap->TileSizeW, GameMap->TileSizeH);
		Hole_14->EditSurface("1440x1080/pngBigHole.png", GameMap->TileSizeW, GameMap->TileSizeH);

		GameFloor = new Tile(Tile::e_Floor, true, "1440x1080/bmpFloor2.bmp");
		GameUpWall = new Tile(Tile::e_UpperWall, false, "1440x1080/bmpUpsideWall.bmp");
		GameLeftWall = new Tile(Tile::e_LeftWall, false, "1440x1080/bmpLeftsideWall.bmp");
		GameRightWall = new Tile(Tile::e_RightWall, false, "1440x1080/bmpRightsideWall.bmp");
		GameDownWall = new Tile(Tile::e_BottomWall, false, "1440x1080/bmpDownsideWall.bmp");
		FirstCorner = new Tile(Tile::e_UpperLCorner, false, "1440x1080/FirstCorner.bmp");
		SecondCorner = new Tile(Tile::e_UpperRCorner, false, "1440x1080/SecondCorner.bmp");
		ThirdCorner = new Tile(Tile::e_BottomLCorner, false, "1440x1080/ThirdCorner.bmp");
		FourthCorner = new Tile(Tile::e_BottomLCorner, false, "1440x1080/FourthCorner.bmp");
		GameUpDoors = new AnimatedTile(AnimatedTile::e_UpperDoors, true, "1440x1080/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameLeftDoors = new AnimatedTile(AnimatedTile::e_LeftDoors, true, "1440x1080/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameRightDoors = new AnimatedTile(AnimatedTile::e_RightDoors, true, "1440x1080/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameDownDoors = new AnimatedTile(AnimatedTile::e_BottomDoors, true, "1440x1080/pngDoors.png", 0, 0, GameMap->TileSizeW, GameMap->TileSizeH);
		GameMenu = new AnimatedTile(AnimatedTile::e_menu, false, "1440x1080/pngGameMenu.png", 0, GameMap->MapH * 2, GameMap->MapW, GameMap->MapH);
		GameEnd = new AnimatedTile(AnimatedTile::e_ending, false, "1440x1080/pngGameEnding.png", 0, 0, GameMap->MapW, 2560);
		GameMenuDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "1440x1080/pngGameMenuDot.png", (GameMap->MapW*0.32), (GameMap->MapH*0.1), DotW, DotH);
		GameMenuPauseDot = new AnimatedTile(AnimatedTile::e_MenuDot, false, "1440x1080/pngGameMenuPauseDot.png", 0, 0, DotW, DotH);
		GameMenuPause = new AnimatedTile(AnimatedTile::e_pause, false, "1440x1080/pngGameMenuPause.png", 0, 0, GameMap->MapW, GameMap->MapH);
		GameResolutions = new AnimatedTile(AnimatedTile::e_menu, false, "1440x1080/pngGameResolutions.png", 0, 480 , 725, (640 / 4));
		FullscreenOnOff = new AnimatedTile(AnimatedTile::e_menu, false, "1440x1080/pngFulscreenOnOff.png", 0, (262 / 2), 578, (262 / 2));
		FloorBlackHole = new Tile(Tile::e_Hole, true, "1440x1080/bmpBlackHole.bmp");
		GameBackground = new Tile(Tile::e_Floor, false, "1440x1080/GameBackground.bmp");
		PlayerStats = new AnimatedTile(Tile::e_Stats, false, "1440x1080/pngStats.png", 0, 0, 128, 48);
		TextBackground = new AnimatedTile(Tile::e_Stats, false, "1440x1080/pngStatsTextBackground.png", 0, 0, 128, 48);


		delete GameWindow;
		GameWindow = new Window("Rogalik", 1400, 1050, false);
		GameWindow->gScreenSurface = SDL_GetWindowSurface(GameWindow->gWindow);
		return;
	}
}

void Game::AddColliders() // funkcja dodaj�ca collidery do obiekt�w
{
	for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
	{
		if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == 100 || GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == 101)
		{
			GameMap->CurrentRoom->RoomObjects[i].ColliderPosX = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosX + 15;
			GameMap->CurrentRoom->RoomObjects[i].ColliderPosY = GameMap->CurrentRoom->RoomObjects[i].RoomObjectPosY + 48;
			GameMap->CurrentRoom->RoomObjects[i].ColliderW = 5;
			GameMap->CurrentRoom->RoomObjects[i].ColliderH = 5;
		}
	}
}

bool Game::IsCollision() // funkcja sprawdzaj�ca, czy zosta�a wykryta kolizja postaci z obiektem
{	
	for (int i = 0; i < GameMap->CurrentRoom->RoomObjects.size(); i++)
	{
		if (Hero->ColliderPosX > GameMap->CurrentRoom->RoomObjects[i].ColliderPosX + GameMap->CurrentRoom->RoomObjects[i].ColliderW ||
			Hero->ColliderPosX + Hero->ColliderW < GameMap->CurrentRoom->RoomObjects[i].ColliderPosX ||
			Hero->ColliderPosY > GameMap->CurrentRoom->RoomObjects[i].ColliderPosY + GameMap->CurrentRoom->RoomObjects[i].ColliderH ||
			Hero->ColliderPosY + Hero->ColliderH < GameMap->CurrentRoom->RoomObjects[i].ColliderPosY)
			continue;

		else
		{
			if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_money)
			{
				Mix_PlayChannel(-1, gCoin, 0);
				Hero->PlayerMoney++;
				GameMap->CurrentRoom->RoomObjects.erase(GameMap->CurrentRoom->RoomObjects.begin() + i);
				return true;
			}
			else if (GameMap->CurrentRoom->RoomObjects[i].RoomObjectID == AnimatedGameObject::e_heart)
			{
				Mix_PlayChannel(-1, gHeart, 1);
				Hero->PlayerHealth++;
				GameMap->CurrentRoom->RoomObjects.erase(GameMap->CurrentRoom->RoomObjects.begin() + i);
				return true;
			}
		}
	}
}