#pragma once
#include <string>
#pragma comment (lib, "SDL2")
#pragma comment (lib, "SDL2_image")
#include <SDL.h>
#include <SDL2\SDL_image.h>

using namespace std;

class Tile
{
public:

	enum E_TileID
	{
		e_Floor,
		e_UpperWall,
		e_LeftWall,
		e_RightWall,
		e_BottomWall,
		e_UpperDoors,
		e_LeftDoors,
		e_RightDoors,
		e_BottomDoors,
		e_UpperLCorner,
		e_UpperRCorner,
		e_BottomLCorner,
		e_BottomRCorner,
		e_menu,
		e_ending,
		e_MenuDot,
		e_pause,
		e_player,
		e_MenuPauseDot,
		e_Hole,
		e_Hole_3x1,
		e_Hole_3x2,
		e_Hole_3x3,
		e_Hole_3x4,
		e_Hole_2x1,
		e_Hole_2x2,
		e_Hole_2x3,
		e_Hole_2x4,
		e_Hole_1x1,
		e_Hole_1x2,
		e_Hole_1x3,
		e_Hole_1x4,
		e_hole_0,
		e_HoleLinkUL,
		e_HoleLinkUR,
		e_HoleLinkDL,
		e_HoleLinkDR,
		e_BigHole,
		e_Stats
	};

	SDL_RWops *rwop = NULL;

	E_TileID TileID;

	Tile();
	virtual ~Tile();

	Tile(E_TileID _tileID, bool _walkable, string _bmpName);

	bool walkable;
	SDL_Surface *TileSurface = NULL;
};