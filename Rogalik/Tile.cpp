#include "Tile.h"

Tile::Tile(E_TileID _tileID, bool _walkable,  string _bmpName)
{
	Tile::TileID = _tileID;
	Tile::walkable = _walkable;

	rwop = SDL_RWFromFile(_bmpName.c_str(), "br");
	Tile::TileSurface = IMG_LoadBMP_RW(rwop);

}

Tile::~Tile()
{
	SDL_RWclose(rwop);
	SDL_FreeSurface(TileSurface);
}